﻿/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
System: Landpower MSCRM 2011
Author: P&L Limted
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
function SetWorksheetStyle() {
    try {

        debugger;
        SetCurrencyEditStyle("pnl_estimatedrevenue");

        // Machine Costs Section
        SetCurrencyEditStyle("pnl_subtotallistprice");
        SetCurrencyEditStyle("pnl_subtotaldealernetprice");
        SetCurrencyEditStyle("pnl_subtotaldiscountpercentage");

        SetCurrencyEditStyle("pnl_totallocallysourceditemslistprice");
        SetCurrencyEditStyle("pnl_totallocallysourceditemsdealernetprice");
        SetCurrencyEditStyle("pnl_totallocallysourceditemsdiscountpercentag");

        SetCurrencyEditStyle("pnl_otherdealercostsfreight");
        SetCurrencyEditStyle("pnl_otherdealercostspredelivery");
        SetCurrencyEditStyle("pnl_otherdealerinterestsubsidy");
        SetCurrencyEditStyle("pnl_totalotherdealercosts");

        SetCurrencyEditStyle("pnl_salesaidapproved");
        SetCurrencyEditStyle("pnl_totallistprice");
        SetCurrencyEditStyle("pnl_scenario1totaldealernetcost");
        SetCurrencyEditStyle("pnl_potentialgpatlistprice");



        // Gross Profit Calculation section
        SetLabelStyle("pnl_scenarioonelabel", true);
        SetLabelStyle("pnl_scenariotwolabel", true);
        SetLabelStyle("pnl_scenariothreelabel", true);

        SetLabelStyle("pnl_dealernetcostlabel", false);
        SetCurrencyEditStyle("pnl_scenario1totaldealernetcost1");
        SetCurrencyEditStyle("pnl_scenario2totaldealernetcost");
        SetCurrencyEditStyle("pnl_scenario3totaldealernetcost");

        SetLabelStyle("pnl_grossprofitpercentagelabel", false);
        SetCurrencyEditStyle("pnl_scenario1grossprofitpercentage");
        SetCurrencyEditStyle("pnl_scenario2grossprofitpercentage");
        SetCurrencyEditStyle("pnl_scenario3grossprofitpercentage");

        SetLabelStyle("pnl_grossprofitdollarlabel", false);
        SetCurrencyEditStyle("pnl_scenario1grossprofitdollar");
        SetCurrencyEditStyle("pnl_scenario2grossprofitdollar");
        SetCurrencyEditStyle("pnl_scenario3grossprofitdollar");

        SetLabelStyle("pnl_netsellingpricegplabel", false);
        SetCurrencyEditStyle("pnl_scenario1netsellingprice");
        SetCurrencyEditStyle("pnl_scenario2netsellingpriceinternal");
        SetCurrencyEditStyle("pnl_scenario3netsellingpriceinternal");

        // Changeover Price Calculation section
        SetLabelStyle("pnl_listpricenewmachineschangeoverlabel", false);
        SetCurrencyEditStyle("pnl_totallistprice1");
        SetCurrencyEditStyle("pnl_totallistprice2");
        SetCurrencyEditStyle("pnl_totallistprice3");

        SetLabelStyle("pnl_lessdiscountschangeoverlabel", false);
        SetCurrencyEditStyle("pnl_scenario1changeoverdiscounts");
        SetCurrencyEditStyle("pnl_scenario2changeoverdiscounts");
        SetCurrencyEditStyle("pnl_scenario3changeoverdiscounts");

        SetLabelStyle("pnl_netsellingpricechangeoverlabel", false);
        SetCurrencyEditStyle("pnl_scenario1netsellingprice1");
        SetCurrencyEditStyle("pnl_scenario2netsellingprice");
        SetCurrencyEditStyle("pnl_scenario3netsellingprice");

        // Trade-Ins section
        // if tradein 1 is empty then s2&3 fields are readonly else writeable
        var readOnly = EditIsEmpty("pnl_tradeinmachine1description");
        SetReadOnly(readOnly, "pnl_tradeinmachine1scenario2value");
        SetReadOnly(readOnly, "pnl_tradeinmachine1scenario3value");

        // if tradein 2 is empty then s2&3 fields are readonly
        var readOnly = EditIsEmpty("pnl_tradeinmachine2description");
        SetReadOnly(readOnly, "pnl_tradeinmachine2scenario2value");
        SetReadOnly(readOnly, "pnl_tradeinmachine2scenario3value");

        // if tradein 3 is empty then s2&3 fields are readonly
        var readOnly = EditIsEmpty("pnl_tradeinmachine3description");
        SetReadOnly(readOnly, "pnl_tradeinmachine3scenario2value");
        SetReadOnly(readOnly, "pnl_tradeinmachine3scenario3value");

        SetLabelStyle("pnl_tradeinmachine1description", false);
        SetCurrencyEditStyle("pnl_tradeinmachine1bookvalue");
        SetCurrencyEditStyle("pnl_tradeinmachine1scenario2value");
        SetCurrencyEditStyle("pnl_tradeinmachine1scenario3value");

        SetLabelStyle("pnl_tradeinmachine2description", false);
        SetCurrencyEditStyle("pnl_tradeinmachine2bookvalue");
        SetCurrencyEditStyle("pnl_tradeinmachine2scenario2value");
        SetCurrencyEditStyle("pnl_tradeinmachine2scenario3value");

        SetLabelStyle("pnl_tradeinmachine3description", false);
        SetCurrencyEditStyle("pnl_tradeinmachine3bookvalue");
        SetCurrencyEditStyle("pnl_tradeinmachine3scenario2value");
        SetCurrencyEditStyle("pnl_tradeinmachine3scenario3value");

        // Totals section
        SetLabelStyle("pnl_totalstradeinlabel", false);
        SetCurrencyEditStyle("pnl_totalbookvalue");
        SetCurrencyEditStyle("pnl_scenario2totaltradedvalue");
        SetCurrencyEditStyle("pnl_scenario3totaltradedvalue");

        SetLabelStyle("pnl_changeoverpricelabel", false);
        SetCurrencyEditStyle("pnl_scenario1changeoverpriceexclgst");
        SetCurrencyEditStyle("pnl_scenario2changeoverpriceexclgst");
        SetCurrencyEditStyle("pnl_scenario3changeoverpriceexclgst");

        SetLabelStyle("pnl_totalsgrossprofitpercentagelabel", false);
        SetCurrencyEditStyle("pnl_scenario1changeovergrossprofitpercentage");
        SetCurrencyEditStyle("pnl_scenario2changeovergrossprofitpercentage");
        SetCurrencyEditStyle("pnl_scenario3changeovergrossprofitpercentage");

        SetLabelStyle("pnl_overtradeamountlabel", false);
        SetCurrencyEditStyle("pnl_scenario2overtradeamount");
        SetCurrencyEditStyle("pnl_scenario3overtradeamount");
    }
    catch (err) {
        alert(err.message + '  NB this Form may not display correctly in browsers other than Internet Explorer.');
    }
}

function EditIsEmpty(editName) {
    var thisEdit = document.getElementById(editName);

    if (thisEdit == null) {
        throw "Failed to locate control: " + editName;
    }
    return (thisEdit.value == "");
}

function SetReadOnly(IsReadOnly, editName) {
    var thisEdit = document.getElementById(editName);

    if (thisEdit == null) {
        throw "Failed to locate control: " + editName;
    }
    thisEdit.disabled = IsReadOnly;
}

function SetLabelStyle(labelName, isRightAligned) {

    var labelEdit = document.getElementById(labelName);

    if (labelEdit == null) {
        throw "Failed to locate control: " + labelName;
    }

    var labelEditContainer = document.getElementById(labelName + "_d");

    labelEdit.disabled = true;
    if (isRightAligned) {
        labelEdit.style.textAlign = "right";
        labelEditContainer.style.width = "20%";
    }
    else {
        labelEdit.style.textAlign = "left";
        labelEditContainer.style.width = "40%";
    }

    labelEdit.style.backgroundColor = "rgb(246, 248, 250)";
    labelEdit.style.borderColor = "rgb(246, 248, 250)";
    labelEdit.style.border = "0px none";

    var parentDiv = labelEdit.parentElement;
    if (parentDiv !== null) {
        parentDiv.style.backgroundColor = "rgb(246, 248, 250)";
        parentDiv.style.borderColor = "rgb(246, 248, 250)";
        parentDiv.style.border = "0px none";
    }
    labelEdit.style.color = "black";
}

function SetCurrencyEditStyle(editName) {

    var thisEdit = document.getElementById(editName);

    if (thisEdit == null) {
        throw "Failed to locate control: " + editName;
    }

    var currencyEditContainer = document.getElementById(editName + "_d");
    currencyEditContainer.style.width = "20%";

    thisEdit.style.textAlign = 'right';

    thisEdit.style.color = "black";
    if (thisEdit.disabled == false) {
        thisEdit.style.borderBottomColor = 'yellow';
        thisEdit.style.backgroundColor = 'yellow';

        var currencySymbolContainer = document.getElementById(editName + "_sym");
        if (currencySymbolContainer != null) {
            currencySymbolContainer.style.borderBottomColor = 'yellow';
            currencySymbolContainer.style.backgroundColor = 'yellow';
        }
    }

    thisEdit.style.display = 'none';
    var redrawFix = thisEdit.offsetHeight;

    thisEdit.style.display = 'block';
}