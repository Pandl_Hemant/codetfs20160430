﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Activities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;

namespace SourceNameandValues
{
    public class app_sourceNames : CodeActivity
    {
        [RequiredArgument]
        [Input("Appraisal")]
        [ReferenceTarget("pnl_appraisal")]
        public InArgument<EntityReference> Appraisal { get; set; }
       
        protected override void Execute(CodeActivityContext executionContext)
        
        {

            ITracingService tracingService = executionContext.GetExtension<ITracingService>();
            IWorkflowContext context = executionContext.GetExtension<IWorkflowContext>();            
            IOrganizationServiceFactory serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
            IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);
            string sourceCode = "";
            //Entity entity = (Entity)context.InputParameters["Target"];
            EntityReference entity = this.Appraisal.Get(executionContext);

            Guid contID = entity.Id;
            ColumnSet contCols = new ColumnSet("pnl_source1name", "pnl_source1value", 
                                               "pnl_source2name", "pnl_source2value", 
                                               "pnl_source3name", "pnl_source3value", 
                                               "pnl_source4name", "pnl_source4value");
            
            var Appraisal  = service.Retrieve("pnl_appraisal", contID, contCols);            
            sourceCode = "SOURCE_1";
            //var fetchxml = GetFetchXml(Appraisal, sourceCode);
            var entityResults = service.RetrieveMultiple(new FetchExpression(GetFetchXml(Appraisal, sourceCode)));
            if (entityResults.Entities == null) return;

            if (entityResults.Entities.Count == 1)
            {

                foreach (var o in entityResults.Entities)
                {
                    if (o.Contains("pnl_displayname"))
                    {
                        Appraisal.Attributes["pnl_source1name"] = o.Attributes["pnl_displayname"].ToString();
                    }
                    if (o.Contains("pnl_value"))
                    {
                        Money value = new Money();
                        value.Value = Convert.ToDecimal(o.Attributes["pnl_value"].ToString());
                        Appraisal.Attributes["pnl_source1value"] = value;
                    }
                    
                }
            }
            sourceCode = "SOURCE_2";

            var entityResults_2 = service.RetrieveMultiple(new FetchExpression(GetFetchXml(Appraisal, sourceCode)));
            if (entityResults_2.Entities == null) return;

            if (entityResults_2.Entities.Count == 1)
            {

                foreach (var o in entityResults_2.Entities)
                { 

                    if (o.Contains("pnl_displayname"))
                    {
                        Appraisal.Attributes["pnl_source2name"] = o.Attributes["pnl_displayname"].ToString();
                    }
                    if (o.Contains("pnl_value"))
                    {
                        Money value = new Money();
                        value.Value = Convert.ToDecimal(o.Attributes["pnl_value"].ToString());
                        Appraisal.Attributes["pnl_source2value"] = value;
                    }

                }
            }
            sourceCode = "SOURCE_3";

            var entityResults_3 = service.RetrieveMultiple(new FetchExpression(GetFetchXml(Appraisal, sourceCode)));
            if (entityResults_3.Entities == null) return;

            if (entityResults_3.Entities.Count == 1)
            {

                foreach (var o in entityResults_3.Entities)
                {
                    if (o.Contains("pnl_displayname"))
                    {
                        Appraisal.Attributes["pnl_source3name"] = o.Attributes["pnl_displayname"].ToString();
                    }
                    

                    if (o.Contains("pnl_value"))
                    {
                        Money value = new Money();
                        value.Value = Convert.ToDecimal(o.Attributes["pnl_value"].ToString());
                        Appraisal.Attributes["pnl_source3value"] = value;
                    }

                }
            }
            sourceCode = "SOURCE_4";

            var entityResults_4 = service.RetrieveMultiple(new FetchExpression(GetFetchXml(Appraisal, sourceCode)));
            if (entityResults_4.Entities == null) return;

            if (entityResults_4.Entities.Count == 1)
            {

                foreach (var o in entityResults_4.Entities)
                {
                    if (o.Contains("pnl_displayname"))
                    {
                        Appraisal.Attributes["pnl_source4name"] = o.Attributes["pnl_displayname"].ToString();
                    }

                    

                    if (o.Contains("pnl_value"))
                    {
                        Money value = new Money();
                        value.Value = Convert.ToDecimal(o.Attributes["pnl_value"].ToString());
                        Appraisal.Attributes["pnl_source4value"] = value;
                    }

                }
            }


            service.Update(Appraisal);

        }       
        private string GetFetchXml(Entity entity, string code)
        {
            string appItem = @"<fetch distinct='false' mapping='logical'>" +
                                                "<entity name='pnl_appraisalitem'>" +
                                                    "<attribute name='pnl_displayname'/>" +
                                                    "<attribute name='pnl_value'/>" +
                                                    "<attribute name='pnl_calccode'/>" +
                                                    "<filter type='and'>" +
                                                          "<condition attribute='pnl_calccode' operator='eq' value='" + code + "'></condition>" +
                                                          "<condition attribute='pnl_appraisalid' operator='eq' uitype='" + entity.LogicalName + "' value='" + entity.Id + "'></condition>" +
                                                          "</filter>" +
                                                        "</entity>" +
                                                       "</fetch>";
            return appItem;
        } 
    }
}
