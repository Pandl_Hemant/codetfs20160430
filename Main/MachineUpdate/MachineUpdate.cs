﻿using LandPower;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;

namespace Landpower.Plugin.MachineUpdate
{
    public class MachineUpdate : IPlugin
    {
        IOrganizationService _service;

        public void Execute(IServiceProvider serviceProvider)
        {

            ITracingService tracingService =
                (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));

            _service = serviceFactory.CreateOrganizationService(context.UserId);
            Entity postEntity = (Entity)context.PostEntityImages["entity"];
            if (context.Depth > 1)
                return;
            crmServiceContext _ctx = new LandPower.crmServiceContext(_service);


            if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
            {
                Entity machineEntity = (Entity)context.InputParameters["Target"];
                if (machineEntity.Contains("pnl_currentcustomerid") && machineEntity.Attributes["pnl_currentcustomerid"] != null)
                {
                    //create Customer-Machine if not exist and close the existing customer-machine records
                    EntityReference currentCustomer = (EntityReference)postEntity.Attributes["pnl_currentcustomerid"];

                    var currCustmachine = (from cm in _ctx.pnl_customermachineSet
                                   where cm.pnl_Machineid.Id.Equals(machineEntity.Id) 
                                   &&    cm.pnl_CustomerId.Id.Equals(currentCustomer.Id)
                                   &&    cm.pnl_EndDate.Equals(null)
                                   select cm).ToList();
                    
                    //if (currCustmachine.Contains<pnl_customermachine>(asd => pnl_customermachine))
                    if(currCustmachine.Count == 0)
                    {                        
                        Entity newCmachine = new Entity("pnl_customermachine");
                        newCmachine.Attributes["pnl_customerid"] = (EntityReference)currentCustomer;
                        newCmachine.Attributes["pnl_machineid"] = (EntityReference)postEntity.ToEntityReference();
                        newCmachine.Attributes["pnl_startdate"] = (DateTime)DateTime.Now;
                        _service.Create(newCmachine);
                    }
                    var cmachine = from cm in _ctx.pnl_customermachineSet
                                   where cm.pnl_Machineid.Id.Equals(machineEntity.Id)                                    
                                   select cm;                    
                        foreach (pnl_customermachine cmac in cmachine)
                        {
                            if (cmac.pnl_CustomerId.Id != currentCustomer.Id && cmac.pnl_EndDate == null)
                            {
                                var updatcmac = new pnl_customermachine
                                {
                                    Id = cmac.Id,
                                    pnl_EndDate = DateTime.Now,
                                };
                                _service.Update(updatcmac);
                            }
                        }
                    }
                //naming convection for machine "year make model"
                if ((machineEntity.Contains("pnl_year") && machineEntity.Attributes["pnl_year"] != null) ||
                    (machineEntity.Contains("pnl_make") && machineEntity.Attributes["pnl_make"] != null) ||
                    (machineEntity.Contains("pnl_model") && machineEntity.Attributes["pnl_model"] != null))
                {
                }
                else
                {
                    Entity postMachine = (Entity)context.PreEntityImages["entity"];
                    if (postMachine.Contains("pnl_name") && postMachine.Attributes["pnl_name"] == null)
                    {
                        Entity machine = new Entity("pnl_machine");
                        machine.A
                    }
                    
                }
                }
            }
        }
}

