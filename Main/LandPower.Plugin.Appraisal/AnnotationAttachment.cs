﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LandPower.Plugin.TradeIn
{
	using System;

	using Microsoft.Xrm.Sdk;

	using BusinessLogic;
	using Data;

	/// <summary>
	/// Prevent photo attachment upload unless the Appraisal is in Inspection.
	/// Prevent multiple photos being attached to an AppraisalPhoto record.
	/// </summary>
	public class AnnotationAttachment : Plugin
	{
		private LocalPluginContext _localPluginContext;
		private EntityReference _objectId;

		public IOrganizationService OrganizationService { get; set; }

		public AnnotationAttachment() : base(typeof (AnnotationAttachment))
		{
			RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Create", "annotation", ExecuteAnnotationCreateUpdate));
			RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Update", "annotation", ExecuteAnnotationCreateUpdate));
		}

		protected void ExecuteAnnotationCreateUpdate(LocalPluginContext localContext)
		{
			if (localContext == null)
			{
				throw new ArgumentNullException("localContext");
			}

			var context = localContext.PluginExecutionContext;
			var inputEntity = (Entity) context.InputParameters["Target"];
			var preEntity = context.PreEntityImages.Contains("preimage") ? context.PreEntityImages["preimage"] : null;
			var annotation = inputEntity.ToEntity<Annotation>();

			_objectId = annotation.ObjectId;
			if (preEntity != null && _objectId == null)
			{
				var preNote = preEntity.ToEntity<Annotation>();
				_objectId = preNote.ObjectId;
			}

			OrganizationService = localContext.OrganizationService;
			_localPluginContext = localContext;

			if (!string.IsNullOrEmpty(annotation.DocumentBody))
			{
				CheckAppraisalIsInInspection(annotation);
			 	CheckDuplicateAttachments(annotation);
			}
		}

		internal void CheckAppraisalIsInInspection(Annotation note)
		{
			if (_objectId == null || _objectId.LogicalName.ToLower() != "pnl_appraisalphoto") return;

			var crmContext = new CrmServiceContext(OrganizationService);
			var appraisalPhoto = crmContext.AppraisalPhoto().FetchAppraisalPhotoById(_objectId.Id);

			if (appraisalPhoto == null) throw new InvalidPluginExecutionException("Unable to retrieve AppraisalPhoto.");

			var appraisalId = appraisalPhoto.pnl_AppraisalId == null ? Guid.Empty : appraisalPhoto.pnl_AppraisalId.Id;

			if (appraisalId == Guid.Empty) throw new InvalidPluginExecutionException("Unable to find Appraisal.");

			var appraisal = crmContext.Appraisal().FetchAppraisalById(appraisalId);

			if (appraisal == null) throw new InvalidPluginExecutionException("Unable to retrieve Appraisal.");

			if (!appraisal.CanUploadPhotos(crmContext))
				throw new InvalidPluginExecutionException("Appraisal must be in Inspection to upload Photos.");
		}

		internal void CheckDuplicateAttachments(Annotation note)
		{
			if (_objectId == null || _objectId.LogicalName.ToLower() != "pnl_appraisalphoto") return;

			var appraisalPhoto = new pnl_appraisalphoto { Id = _objectId.Id };
			var photoCount = appraisalPhoto.GetPhotoCount(new CrmServiceContext(OrganizationService));
			
			if (photoCount > 2)
				throw new InvalidPluginExecutionException("Only one attachment is allowed for an AppraisalPhoto.");
		}
	}
}
