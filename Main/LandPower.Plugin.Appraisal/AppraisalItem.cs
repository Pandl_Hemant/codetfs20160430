using LandPower.BusinessLogic;

namespace LandPower.Plugin.TradeIn
{
    using System;

    using Microsoft.Xrm.Sdk;

    using Data;

    // Check appraisal update is permitted.
    // Updates the parent Appraisal to cause summary values to recalculate.
    public class AppraisalItem : Plugin
    {
        private const string PostImageAlias = "postimage";
        private const string PreImageAlias = "preimage";
        private IOrganizationService _organizationService;
        private LocalPluginContext _localPluginContext;
        private Guid _initiatingUserId;

        public AppraisalItem()
            : base(typeof(AppraisalItem))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "SetState", "pnl_appraisalitem", ExecuteAppraisalItemUpdate));
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "SetStateDynamicEntity", "pnl_appraisalitem", ExecuteAppraisalItemUpdate));
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Create", "pnl_appraisalitem", ExecuteAppraisalItemUpdate));
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Update", "pnl_appraisalitem", ExecuteAppraisalItemUpdateCheck));
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update", "pnl_appraisalitem", ExecuteAppraisalItemUpdate));
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Delete", "pnl_appraisalitem", ExecuteAppraisalItemDelete));
        }

        protected void ExecuteAppraisalItemUpdate(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException("localContext");
            }

            var context = localContext.PluginExecutionContext;
            var postImageEntity = (context.PostEntityImages != null && context.PostEntityImages.Contains(PostImageAlias)) ? context.PostEntityImages[PostImageAlias] : null;

            if (context.IsOfflinePlayback || context.Depth >= 3) return; // No need to re-calculate average valution if/when (Outlook) clients run offline.
            if (null == postImageEntity) return;

            _organizationService = localContext.OrganizationService;
            _localPluginContext = localContext;
            _initiatingUserId = context.InitiatingUserId;

            var appraisalItem = postImageEntity.ToEntity<pnl_appraisalitem>();
            if (appraisalItem.pnl_AppraisalId != null)
            {
                CheckCanUpdateAppraisal(appraisalItem.pnl_AppraisalId.Id);
            }

         
            TryToUpdateAppraisal(appraisalItem);
        }



        protected void ExecuteAppraisalItemDelete(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException("localContext");
            }

            var context = localContext.PluginExecutionContext;
            var preImageEntity = (context.PreEntityImages != null && context.PreEntityImages.Contains(PreImageAlias)) ? context.PreEntityImages[PreImageAlias] : null;

            if (null == preImageEntity || context.Depth > 3 ) return;

            _organizationService = localContext.OrganizationService;
            _localPluginContext = localContext;
            _initiatingUserId = context.InitiatingUserId;

            var appraisalItem = preImageEntity.ToEntity<pnl_appraisalitem>();
            if (appraisalItem.pnl_AppraisalId != null)
            {
                CheckCanUpdateAppraisal(appraisalItem.pnl_AppraisalId.Id);
            }
            TryToUpdateAppraisal(appraisalItem);
        }

        protected void ExecuteAppraisalItemUpdateCheck(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException("localContext");
            }

            var context = localContext.PluginExecutionContext;
            var preImageEntity = (context.PreEntityImages != null && context.PreEntityImages.Contains(PreImageAlias)) ? context.PreEntityImages[PreImageAlias] : null;
            var postImageEntity = (context.PostEntityImages != null && context.PostEntityImages.Contains(PostImageAlias)) ? context.PostEntityImages[PostImageAlias] : null;

            if (context.Depth > 3) return;

            _organizationService = localContext.OrganizationService;
            _initiatingUserId = context.InitiatingUserId;

            // Use preimage on update and also the inputParams so that appraisal cannot be changed as a workaround.
            var appraisalItem = preImageEntity == null ? postImageEntity == null ? null : postImageEntity.ToEntity<pnl_appraisalitem>() : preImageEntity.ToEntity<pnl_appraisalitem>();
            if (null == appraisalItem) return;

            if (appraisalItem.pnl_AppraisalId == null) return;

            CheckCanUpdateAppraisal(appraisalItem.pnl_AppraisalId.Id);

            var inputEntity = context.InputParameters.Contains("Target") ? (Entity)context.InputParameters["Target"] : null;
            if (null == inputEntity || !inputEntity.Contains("pnl_appraisalid")) return;

            var inputAppraisalId = ((EntityReference)inputEntity["pnl_appraisalid"]).Id;
            if (inputAppraisalId == appraisalItem.pnl_AppraisalId.Id) return;
            CheckCanUpdateAppraisal(inputAppraisalId);
        }

        internal void CheckCanUpdateAppraisal(Guid appraisalId)
        {
            var context = new CrmServiceContext(_organizationService);
            var appraisal = context.Appraisal().FetchAppraisalById(appraisalId);
            var canUpdate = appraisal.CanApplyChanges(_initiatingUserId, context);

            if (canUpdate) return;

            throw new InvalidPluginExecutionException("Permission denied. The Appraisal can be updated if it is in inspection or you are an approver and it is waiting for approval.");
        }

        internal void TryToUpdateAppraisal(pnl_appraisalitem appraisalItem)
        {
            try
            {
                UpdateAppraisalNameAndLastSavedFromAppraisalItem(appraisalItem);
            }
            catch (Exception ex)
            {
                _localPluginContext.Trace("Inner Exception: " + ex.InnerException);
                _localPluginContext.Trace("StackTrace: " + ex.StackTrace);
                throw new InvalidPluginExecutionException(string.Format("PostAppraisalItemUpdatePlugin exception: {0}", ex.Message));
            }
        }

        internal void UpdateAppraisalNameAndLastSavedFromAppraisalItem(pnl_appraisalitem appraisalitem)
        {
            if (null == appraisalitem.pnl_AppraisalId) return;
            _localPluginContext.Trace(string.Format("Entered UpdateAppraisalNameAndLastSavedFromAppraisalItem for Appraisal {0}...", appraisalitem.pnl_AppraisalId));

            var appraisalId = appraisalitem.pnl_AppraisalId.Id;
            var context = new CrmServiceContext(_organizationService);
            var existingAppraisal = context.Appraisal().FetchAppraisalById(appraisalId);
            var existingName = existingAppraisal.pnl_name;
            var name = existingAppraisal.GenerateName(context);

            var appraisal = new pnl_appraisal
            {
                Id = appraisalId,
                pnl_lastsavedfromappraisalitemon = DateTime.Now
            };
            if (name != existingName)
            {
                appraisal.pnl_name = name;
                appraisal.Attributes.Add("pnl_forceappraisalitemgroupnamefieldupdate", DateTime.Now);
                //appraisal.Attributes.Add("pnl_source1name",existingAppraisal.pnl_va
            }

           
                
               
           


            _localPluginContext.Trace(string.Format("Updating Appraisal [{0}] name to '{1}'...", appraisalId, name));
            _organizationService.Update(appraisal.ToEntity<Entity>());
        }
    }
}
