﻿using LandPower.BusinessLogic;
using LandPower.Data;

namespace LandPower.Plugin.TradeIn
{
	using System;

	using Microsoft.Xrm.Sdk;

	public class AppraisalEdit : Plugin
	{
		private Guid _initiatingUserId;
		private Entity _preImageEntity;
		private Entity _postImageEntity;
		private Entity _inputEntity;

		public IOrganizationService OrganizationService { get; set; }

		public AppraisalEdit()
			: base(typeof(AppraisalEdit))
		{
			// Not necessary to check editability on create.
			RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Update", "pnl_appraisal", ExecuteAppraisalEditCheck));
			RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Delete", "pnl_appraisal", ExecuteAppraisalEditCheck));
			// SetStateDynamicEntity causes "The plug-in execution failed because the Sandbox Client encountered an error during initialization" error.
			// Also same error if update step registered on state/status attributes.
		}

		/// <summary>
		/// Prevent edits unless the Appraisal status is Inspection or is Waiting for Approval and the user is a member of the Approvers team.
		/// Also pushes summary values/costs back down to the appropriate appraisal item.
		/// </summary>
		protected void ExecuteAppraisalEditCheck(LocalPluginContext localContext)
		{
			if (localContext == null)
			{
				throw new ArgumentNullException("localContext");
			}

			var context = localContext.PluginExecutionContext;

			if (context.Depth > 3) return;

			_preImageEntity = (context.PreEntityImages != null && context.PreEntityImages.Contains("preimage"))
				? context.PreEntityImages["preimage"] : null;
			_postImageEntity = (context.PostEntityImages != null && context.PostEntityImages.Contains("postimage"))
				? context.PostEntityImages["postimage"] : null;

			OrganizationService = localContext.OrganizationService;
			_initiatingUserId = context.InitiatingUserId;

			var appraisal = _preImageEntity == null
				? _postImageEntity == null ? null : _postImageEntity.ToEntity<pnl_appraisal>()
				: _preImageEntity.ToEntity<pnl_appraisal>();
			if (null == appraisal) return;

			var canUpdate = appraisal.CanApplyChanges(_initiatingUserId, new CrmServiceContext(OrganizationService));
			if (!canUpdate)
			{
				const string message =
					"Permission denied. The Appraisal can be updated if it is in inspection, or you are an approver and it is waiting for approval, " +
						"or you are a System Administrator and it has been Approved.";
				throw new InvalidPluginExecutionException(message);
			}

			if (context.MessageName.ToLower() == "delete" || _preImageEntity == null) return;
			_inputEntity = (Entity)context.InputParameters["Target"];
			if (_inputEntity.Contains("statuscode"))
			{
				var preStatus = _preImageEntity.GetAttributeValue<OptionSetValue>("statuscode");
				var status = _inputEntity.GetAttributeValue<OptionSetValue>("statuscode");
                if (status.Value == (int)pnl_appraisal_statuscode.Inspection_1)
                {
                    // Reset fields so the appraisal can be reappraised.
                    //SetInputAttribute(_inputEntity, "pnl_appraisaldate", null);
                    SetInputAttribute(_inputEntity, "pnl_approvedtradeprice", new Money(0m));
                    SetInputAttribute(_inputEntity, "pnl_approvalrequestedon", null);
                    SetInputAttribute(_inputEntity, "pnl_valuationapprovedby", null);
                    SetInputAttribute(_inputEntity, "pnl_valuationapprovedon", null);
                    SetInputAttribute(_inputEntity, "pnl_primaryapprover", null);

                    _preImageEntity.ToEntity<pnl_appraisal>().SetAppraisalItemsReadOnly(OrganizationService, false);
                }
				if (preStatus.Value == (int)pnl_appraisal_statuscode.ApprovedApprasial_125760001 && status.Value != preStatus.Value)
				{
					// Note, this conditional check is made separately to check above because the plug-in will run again on status reason change.
					if (status.Value == (int)pnl_appraisal_statuscode.Inspection_1)
					{
						// Reset fields so the appraisal can be reappraised.
						//SetInputAttribute(_inputEntity, "pnl_appraisaldate", null);
						SetInputAttribute(_inputEntity, "pnl_approvedtradeprice", new Money(0m));
						SetInputAttribute(_inputEntity, "pnl_approvalrequestedon", null);
						SetInputAttribute(_inputEntity, "pnl_valuationapprovedby", null);
						SetInputAttribute(_inputEntity, "pnl_valuationapprovedon", null);
						SetInputAttribute(_inputEntity, "pnl_primaryapprover", null);

						_preImageEntity.ToEntity<pnl_appraisal>().SetAppraisalItemsReadOnly(OrganizationService, false);
					}
                    // Note, this conditional check is made separately to check above because the plug-in will run again on status reason change.
                    else if (status.Value == (int)pnl_appraisal_statuscode.InspectionWaitingforApproval)
                    {
                        // Reset fields so the appraisal can be reapproved.
                        //SetInputAttribute(_inputEntity, "pnl_appraisaldate", null);
                        /*
                        SetInputAttribute(_inputEntity, "pnl_approvalrequestedon", null);
                         */
                        SetInputAttribute(_inputEntity, "pnl_approvedtradeprice", new Money(0m));
                        SetInputAttribute(_inputEntity, "pnl_valuationapprovedby", null);
                        SetInputAttribute(_inputEntity, "pnl_valuationapprovedon", null);
                        SetInputAttribute(_inputEntity, "pnl_primaryapprover", null);
                        //change made by ranjith
                        // start comment
                        //_preImageEntity.ToEntity<pnl_appraisal>().SetAppraisalItemsReadOnly(OrganizationService, false);
                        //end comment
                        _preImageEntity.ToEntity<pnl_appraisal>().SetAppraisalItemsReadOnly(OrganizationService);
                    }					
					else
					{
						const string message = "An approved Appraisal may only have its Status Reason changed to \"Inspection\" or \"Inspection - Awaiting Approval\".";
						throw new InvalidPluginExecutionException(message);
					}
				}

				if (preStatus.Value == (int) pnl_appraisal_statuscode.Inspection_1 && status.Value != preStatus.Value)
				{
					// Set appraisalitems readonly when the appraisal leaves the inspection status.
					_preImageEntity.ToEntity<pnl_appraisal>().SetAppraisalItemsReadOnly(OrganizationService);
				}
			}

			UpdateAppraisalItemValues(context.PrimaryEntityId); // Update appraisalitems if summary/cost value has changed
		}

		internal void UpdateAppraisalItemValues(Guid appraisalId)
		{
			var calculatedAverage = GetAttributeValueFromInputOrPreImage("pnl_calculatedaverage");
			var totalCosts = GetAttributeValueFromInputOrPreImage("pnl_totalcosts");
			var expectedRetainedMarginDollar = GetAttributeValueFromInputOrPreImage("pnl_expectedretainedmargindollar");
			// TODO: Fix pnl_calcuatedtradeprice attribute name when HK updates CRM customizations.
			var calculatedTradePrice = GetAttributeValueFromInputOrPreImage("pnl_calcuatedtradeprice");
			var repairCost = GetAttributeValueFromInputOrPreImage("pnl_repaircosts");
			var freightCost = GetAttributeValueFromInputOrPreImage("pnl_freight");
			var preDeliveryExpense = GetAttributeValueFromInputOrPreImage("pnl_predeliveryexpenses");
			var otherCosts = GetAttributeValueFromInputOrPreImage("pnl_othercosts");
			var appraisal = new pnl_appraisal {Id = appraisalId};
			
			appraisal.UpdateAppraisalItemValue("Calculated Average", calculatedAverage, OrganizationService, "Valuation");
			appraisal.UpdateAppraisalItemValue("Total Costs", totalCosts, OrganizationService);
			appraisal.UpdateAppraisalItemValue("Expected Retained Margin", expectedRetainedMarginDollar, OrganizationService);
			appraisal.UpdateAppraisalItemValue("Calculated Trade Price", calculatedTradePrice, OrganizationService);
			appraisal.UpdateAppraisalItemValue("Repair Costs", repairCost, OrganizationService);
			appraisal.UpdateAppraisalItemValue("Freight", freightCost, OrganizationService);
			appraisal.UpdateAppraisalItemValue("PD Expenses", preDeliveryExpense, OrganizationService);
			appraisal.UpdateAppraisalItemValue("Other", otherCosts, OrganizationService);
		}

		internal Money GetAttributeValueFromInputOrPreImage(string attributeName)
		{
			var value = _inputEntity.GetAttributeValue<Money>(attributeName) ??
			            _preImageEntity.GetAttributeValue<Money>(attributeName);

			return value;
		}

		internal void SetInputAttribute(Entity inputEntity, string attributeName, object attributeValue)
		{
			if (inputEntity.Attributes.Contains(attributeName))
			{
				inputEntity.Attributes[attributeName] = attributeValue;
			}
			else
			{
				inputEntity.Attributes.Add(attributeName, attributeValue);
			}
		}
	}
}