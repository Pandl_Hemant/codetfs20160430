﻿using System;
using LandPower.BusinessLogic;
using LandPower.Data;

namespace LandPower.Plugin.TradeIn
{
	using Microsoft.Xrm.Sdk;

	public class AppraisalApprovedPrice : Plugin
	{
		internal IOrganizationService OrganizationService { get; set; }
		
		public AppraisalApprovedPrice() : base(typeof(AppraisalApprovedPrice))
		{
			RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update", "pnl_appraisal", ExecuteApprovedPrice));
		}

		protected void ExecuteApprovedPrice(LocalPluginContext localContext)
		{
			if (localContext == null)
			{
				throw new ArgumentNullException("localContext");
			}

			var context = localContext.PluginExecutionContext;
			var postImageEntity = context.PostEntityImages["postimage"];
			
			if (context.Depth > 3) return;

			OrganizationService = localContext.OrganizationService;

			var appraisal = postImageEntity.ToEntity<pnl_appraisal>();
			CalculateActualRetainedMargin(appraisal);
			PushApprovedTradePriceToAppraisalItem(appraisal);
		}

		internal void CalculateActualRetainedMargin(pnl_appraisal appraisal)
		{
			var actualRetainedMarginPercent = appraisal.GetActualRetainedMarginPercentage();
			var entity = new Entity("pnl_appraisal") {Id = appraisal.Id};
			entity.Attributes.Add("pnl_actualretainedmarginpercentage", actualRetainedMarginPercent);
			OrganizationService.Update(entity);
		}

		internal void PushApprovedTradePriceToAppraisalItem(pnl_appraisal appraisal)
		{
			var approvedTradePrice = appraisal.pnl_ApprovedTradePrice == null ? 0m : Math.Round(appraisal.pnl_ApprovedTradePrice.Value, 0, MidpointRounding.AwayFromZero);
			appraisal.UpdateAppraisalItemValue("Approved Trade Price", new Money(approvedTradePrice), OrganizationService);
		}
	}
}
