﻿using System;
using System.Linq;

namespace LandPower.Plugin.TradeIn
{
	using Microsoft.Xrm.Sdk;

	using Data;

	/// <summary>
	/// Ensure the AppraisalItemGroup names regenerate when the Appraisal name changes.
	/// </summary>
	public class AppraisalUpdateAppraisalItemGroupFieldName : Plugin
	{
		internal IOrganizationService OrganizationService { get; set; }

		public AppraisalUpdateAppraisalItemGroupFieldName() : base(typeof (AppraisalUpdateAppraisalItemGroupFieldName))
		{
			RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update", "pnl_appraisal", ExecuteAppraisalUpdateAppraisalItemGroupFieldName));
		}

		protected void ExecuteAppraisalUpdateAppraisalItemGroupFieldName(LocalPluginContext localContext)
		{
			if (localContext == null)
			{
				throw new ArgumentNullException("localContext");
			}

			var context = localContext.PluginExecutionContext;

			if (context.Depth > 3) return;

			OrganizationService = localContext.OrganizationService;
			UpdateAppraisalItemGroupNames(context.PrimaryEntityId);
		}

		internal void UpdateAppraisalItemGroupNames(Guid appraisalId)
		{
			var context = new CrmServiceContext(OrganizationService);
			var itemGroups = context.Appraisal().FetchAppraisalItemGroups(appraisalId).ToList();

			foreach (var itemGroup in itemGroups)
			{ 
				var entity = new Entity("pnl_appraisalitemgroup") {Id = itemGroup.Id};
				entity.Attributes.Add("pnl_name", DateTime.UtcNow.ToString("O"));
				OrganizationService.Update(entity);
			}
		}
	}
}
