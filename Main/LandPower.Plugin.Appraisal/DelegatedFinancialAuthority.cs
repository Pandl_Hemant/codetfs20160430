﻿namespace LandPower.Plugin.TradeIn
{
	using System;
	using System.Linq;

	using Microsoft.Crm.Sdk.Messages;
	using Microsoft.Xrm.Sdk;

	using Data;

	// Email notification of which values have changed.
	public class DelegatedFinancialAuthority : Plugin
	{
		private LocalPluginContext _localPluginContext;
		private const string EmailTo = "iDEALDFAChanges@landpower.co.nz";
		public IOrganizationService OrganizationService { get; set; }
		private DfaHelper _dfaHelper;

		public DelegatedFinancialAuthority() : base(typeof(DelegatedFinancialAuthority))
		{
			RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Create", "pnl_delegatedfinancialauthority", ExecuteDfaUpdate));
			RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Update", "pnl_delegatedfinancialauthority", ExecuteDfaUpdate));
			RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Delete", "pnl_delegatedfinancialauthority", ExecuteDfaUpdate));
		}

		protected void ExecuteDfaUpdate(LocalPluginContext localContext)
		{
			if (localContext == null)
			{
				throw new ArgumentNullException("localContext");
			}
			_localPluginContext = localContext;

			var context = localContext.PluginExecutionContext;
			// Target is type EntityReference for delete message. We don't need to use the input parameters for delete.
			var inputEntity = context.MessageName.ToLower() == "delete" ? new Entity("pnl_delegatedfinancialauthority") : (Entity)context.InputParameters["Target"];
			var preImageEntity = context.PreEntityImages.Contains("preimage") ? context.PreEntityImages["preimage"] : null;
			var messageName = context.MessageName.ToLower();

			if (context.Depth > 3) return;

			OrganizationService = localContext.OrganizationService;
			_dfaHelper = new DfaHelper(OrganizationService);

			if (IsDuplicate(messageName, preImageEntity, inputEntity, context.PrimaryEntityId))
			{
				throw new InvalidPluginExecutionException("A record with the same Sales Rep and Type already exists. Please enter different values.");
			}

			if (messageName == "delete")
			{
				TryToCheckDfaDelete(preImageEntity);
			}
			else
			{
				// We can get the primaryentityid post create but pass guid.empty so email knows not to set regardingobjectid (since the DFA record
				// does not exist until after this plug-in has finished executing).
				TryToCheckDfaChanges(inputEntity, preImageEntity,
					context.MessageName.ToLower() == "create" ? Guid.Empty : context.PrimaryEntityId);
			}
		}

		internal bool IsDuplicate(string messageName, Entity preImageEntity, Entity inputEntity, Guid currentRecordId)
		{
			EntityReference salesRep;
			var xrmContext = new CrmServiceContext(OrganizationService);

			switch (messageName.ToLower())
			{
				case "create":
					salesRep = inputEntity.GetAttributeValue<EntityReference>("pnl_salesrepid");
					var dfaType = inputEntity.GetAttributeValue<OptionSetValue>("pnl_type");
					return (salesRep != null && dfaType != null) && xrmContext.DelegatedFinancialAuthority().IsDuplicate(salesRep.Id, dfaType.Value, currentRecordId);
				case "update":
					salesRep = inputEntity.Contains("pnl_salesrepid")
						? inputEntity.GetAttributeValue<EntityReference>("pnl_salesrepid")
						: preImageEntity.GetAttributeValue<EntityReference>("pnl_salesrepid");
					dfaType = inputEntity.Contains("pnl_type")
						? inputEntity.GetAttributeValue<OptionSetValue>("pnl_type")
						: preImageEntity.GetAttributeValue<OptionSetValue>("pnl_type");
					return (salesRep != null && dfaType != null) && xrmContext.DelegatedFinancialAuthority().IsDuplicate(salesRep.Id, dfaType.Value, currentRecordId);
				default: // delete
					return false;
			}
		}

		public void TryToCheckDfaChanges(Entity inputEntity, Entity preImageEntity, Guid dfaId)
		{
			try
			{
				var changedAttributes = _dfaHelper.GetChangedAttributes(inputEntity, preImageEntity).ToList();
				if (!changedAttributes.Any()) return;
				var emailBody = string.Format("The following changes have been made to the DFA record by {0}:<br/><br/>{1}",
					_dfaHelper.GetEntityReferenceName((EntityReference)inputEntity["modifiedby"]),
					_dfaHelper.GetChangedValueTable(changedAttributes, "pnl_delegatedfinancialauthority"));
				var dfaName = inputEntity.Contains("pnl_salesrepid")
					? _dfaHelper.GetEntityReferenceName((EntityReference) inputEntity["pnl_salesrepid"])
					: preImageEntity != null && preImageEntity.Contains("pnl_salesrepid")
						? _dfaHelper.GetEntityReferenceName((EntityReference) preImageEntity["pnl_salesrepid"])
						: "<Not Specified>";
				CreateAndSendEmail(_localPluginContext.PluginExecutionContext.InitiatingUserId, emailBody, dfaName, dfaId);
			}
			catch (Exception ex)
			{
				_localPluginContext.Trace("Inner Exception: " + ex.InnerException);
				_localPluginContext.Trace("StackTrace: " + ex.StackTrace);
				throw new InvalidPluginExecutionException(string.Format("DelegatedFinancialAuthorityUpdatePlugin exception: {0}", ex.Message));
			}
		}

		public void TryToCheckDfaDelete(Entity preImageEntity)
		{
			try
			{
				var dfaId = preImageEntity.Id;
				var dfaName = preImageEntity.Contains("pnl_name") ? preImageEntity["pnl_name"].ToString() : preImageEntity.Contains("pnl_salesrepid") ? 
					_dfaHelper.GetEntityReferenceName((EntityReference)preImageEntity["pnl_salesrepid"]) : "<Not Specified>";
				var initiatingUserId = _localPluginContext.PluginExecutionContext.InitiatingUserId;
				var emailBody = string.Format("The DFA record, {0} ({1}), was deleted by {2}.",
					dfaName, dfaId, _dfaHelper.GetEntityReferenceName(new EntityReference("systemuser", initiatingUserId)));
				CreateAndSendEmail(initiatingUserId, emailBody, dfaName, Guid.Empty);
			}
			catch (Exception ex)
			{
				_localPluginContext.Trace("Inner Exception: " + ex.InnerException);
				_localPluginContext.Trace("StackTrace: " + ex.StackTrace);
				throw new InvalidPluginExecutionException(string.Format("DelegatedFinancialAuthorityDeletePlugin exception: {0}", ex.Message));
			}
		}

		internal void CreateAndSendEmail(Guid fromUserId, string body, string dfaName, Guid dfaId)
		{
			var subject = "Change Notification for Delegated Financial Authority: " + dfaName;
			var email = new Email
			{
				To = new[] { new ActivityParty{AddressUsed = EmailTo} },
				From = new[] { new ActivityParty { PartyId = new EntityReference("systemuser", fromUserId) } },
				Subject = subject,
				Description = body,
				DirectionCode = true // Outgoing
			};
			if (dfaId != Guid.Empty)
			{
				email.RegardingObjectId = new EntityReference("pnl_delegatedfinancialauthority", dfaId);
			}
			var emailId = OrganizationService.Create(email);

			SendEmail(emailId);
		}

		internal void SendEmail(Guid emailId)
		{
			var request = new SendEmailRequest
			{
				EmailId = emailId,
				TrackingToken = "",
				IssueSend = true
			};

			OrganizationService.Execute(request);
		}
	}
}