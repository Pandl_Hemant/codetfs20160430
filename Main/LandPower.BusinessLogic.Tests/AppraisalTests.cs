﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using LandPower.Data;

using Microsoft.Xrm.Sdk;
using Moq;

namespace LandPower.BusinessLogic.Tests
{
	[TestClass]
	public class AppraisalTests
	{
		private const string SalesRepId = "00000000-0000-0000-0000-000000000000";
		private const string Approver1Id = "A7903ACC-640F-E311-8BEA-1CC1DE79B4CA"; // Peter Marshall
		private const string Approver2Id = "6E7F2015-E331-E211-9DF3-1CC1DE79B4CA"; // Roger Nehoff
		private const string Approver3Id = "6E7F2015-E331-E211-9DF3-1CC1DE79B4CA"; // Roger Nehoff
		private const string Approver4Id = "20DCD63E-0F14-E311-84C0-1CC1DE6E6BAE"; // Richard Wilson
		private readonly Guid _comparative1Id = new Guid("35994032-513D-E411-80F4-0050568A79C5");
		private readonly Guid _comparative2Id = new Guid("31EFBE2A-513D-E411-80F4-0050568A79C5");

		[TestMethod]
		public void GetActivityPartiesForApprovalLevel1Test()
		{
			var salesRepId = new Guid(SalesRepId);
			var target = new pnl_appraisal
			{
				OwnerId = new EntityReference("systemuser", salesRepId),
				pnl_RecommendedTradePrice = new Money(27459m)
			};
			var mockService = new Mock<IOrganizationService> {CallBase = false};
			var mockContext = GetMockServiceContext(mockService.Object);
			// Reset the user factory back since the away tests run first and mock it.
			SystemUserExtensions.UserFactory = context => new UserExtensions(mockContext.Object); 
			
			var actual = target.GetActivityPartiesForApproval(mockContext.Object).ToList();

			Assert.AreEqual(1, actual.Count);
			Assert.AreEqual("Peter Marshall", actual[0].PartyId.Name);
			Assert.AreEqual(activityparty_participationtypemask.ToRecipient, actual[0].ParticipationTypeMaskEnum);
		}

		[TestMethod]
		public void GetActivityPartiesForApprovalLevel1AwayTest()
		{
			var salesRepId = new Guid(SalesRepId);
			var awayUserId = new Guid(Approver1Id);
			var target = new pnl_appraisal
			{
				OwnerId = new EntityReference("systemuser", salesRepId),
				pnl_RecommendedTradePrice = new Money(27459m)
			};
			var mockService = new Mock<IOrganizationService> { CallBase = false };
			var mockContext = GetMockServiceContext(mockService.Object);
			var mockUserExtensions = new Mock<IUserExtensions> { CallBase = true };
			mockUserExtensions.Setup(u => u.FetchSystemUserById(It.Is<Guid>(p => p == awayUserId)))
				.Returns((Guid id) => new SystemUser { pnl_isaway = true, Id = id });
			mockUserExtensions.Setup(u => u.FetchSystemUserById(It.Is<Guid>(p => p != awayUserId)))
				.Returns((Guid id) => new SystemUser { pnl_isaway = false, Id = id });

			SystemUserExtensions.UserFactory = context => mockUserExtensions.Object; // Replace the factory with one that returns the mock.

			var actual = target.GetActivityPartiesForApproval(mockContext.Object).ToList();

			Assert.AreEqual(2, actual.Count);
			Assert.AreEqual("Roger Nehoff", mockContext.Object.SystemUserSet.Single(u => u.Id == actual[0].PartyId.Id).FullName);
			Assert.AreEqual(activityparty_participationtypemask.ToRecipient, actual[0].ParticipationTypeMaskEnum);
			Assert.AreEqual("Peter Marshall", mockContext.Object.SystemUserSet.Single(u => u.Id == actual[1].PartyId.Id).FullName);
			Assert.AreEqual(activityparty_participationtypemask.CCRecipient, actual[1].ParticipationTypeMaskEnum);
		}

		[TestMethod]
		public void GetActivityPartiesForApprovalLevel2Test()
		{
			var salesRepId = new Guid(SalesRepId);
			var target = new pnl_appraisal
			{
				OwnerId = new EntityReference("systemuser", salesRepId),
				pnl_RecommendedTradePrice = new Money(64459m)
			};
			var mockService = new Mock<IOrganizationService> { CallBase = false };
			var mockContext = GetMockServiceContext(mockService.Object);
			// Reset the user factory back since the away tests run first and mock it.
			SystemUserExtensions.UserFactory = context => new UserExtensions(mockContext.Object); 
			
			var actual = target.GetActivityPartiesForApproval(mockContext.Object).ToList();

			Assert.AreEqual(2, actual.Count);
			Assert.AreEqual("Roger Nehoff", actual[0].PartyId.Name);
			Assert.AreEqual(activityparty_participationtypemask.ToRecipient, actual[0].ParticipationTypeMaskEnum);
			Assert.AreEqual("Peter Marshall", actual[1].PartyId.Name);
			Assert.AreEqual(activityparty_participationtypemask.CCRecipient, actual[1].ParticipationTypeMaskEnum);
		}

		[TestMethod]
		public void GetActivityPartiesForApprovalLevel2AwayTest()
		{
			var salesRepId = new Guid(SalesRepId);
			var awayUserId = new Guid(Approver2Id);
			var target = new pnl_appraisal
			{
				OwnerId = new EntityReference("systemuser", salesRepId),
				pnl_RecommendedTradePrice = new Money(67459m)
			};
			var mockService = new Mock<IOrganizationService> { CallBase = false };
			var mockContext = GetMockServiceContext(mockService.Object);
			var mockUserExtensions = new Mock<IUserExtensions> { CallBase = true };
			mockUserExtensions.Setup(u => u.FetchSystemUserById(It.Is<Guid>(p => p == awayUserId)))
				.Returns((Guid id) => new SystemUser { pnl_isaway = true, Id = id });
			mockUserExtensions.Setup(u => u.FetchSystemUserById(It.Is<Guid>(p => p != awayUserId)))
				.Returns((Guid id) => new SystemUser { pnl_isaway = false, Id = id });

			SystemUserExtensions.UserFactory = context => mockUserExtensions.Object; // Replace the factory with one that returns the mock.

			var actual = target.GetActivityPartiesForApproval(mockContext.Object).ToList();

			Assert.AreEqual(3, actual.Count);
			Assert.AreEqual("Richard Wilson", mockContext.Object.SystemUserSet.Single(u => u.Id == actual[0].PartyId.Id).FullName);
			Assert.AreEqual(activityparty_participationtypemask.ToRecipient, actual[0].ParticipationTypeMaskEnum);
			Assert.AreEqual("Peter Marshall", mockContext.Object.SystemUserSet.Single(u => u.Id == actual[1].PartyId.Id).FullName);
			Assert.AreEqual(activityparty_participationtypemask.CCRecipient, actual[1].ParticipationTypeMaskEnum);
			Assert.AreEqual("Roger Nehoff", mockContext.Object.SystemUserSet.Single(u => u.Id == actual[2].PartyId.Id).FullName);
			Assert.AreEqual(activityparty_participationtypemask.CCRecipient, actual[2].ParticipationTypeMaskEnum);
		}

		[TestMethod]
		public void GetActivityPartiesForApprovalLevel3Test()
		{
			var salesRepId = new Guid(SalesRepId);
			var target = new pnl_appraisal
			{
				OwnerId = new EntityReference("systemuser", salesRepId),
				pnl_RecommendedTradePrice = new Money(124459m)
			};
			var mockService = new Mock<IOrganizationService> { CallBase = false };
			var mockContext = GetMockServiceContext(mockService.Object);
			// Reset the user factory back since the away tests run first and mock it.
			SystemUserExtensions.UserFactory = context => new UserExtensions(mockContext.Object); 
			
			var actual = target.GetActivityPartiesForApproval(mockContext.Object).ToList();

			Assert.AreEqual(2, actual.Count);
			Assert.AreEqual("Roger Nehoff", actual[0].PartyId.Name);
			Assert.AreEqual(activityparty_participationtypemask.ToRecipient, actual[0].ParticipationTypeMaskEnum);
			Assert.AreEqual("Peter Marshall", actual[1].PartyId.Name);
			Assert.AreEqual(activityparty_participationtypemask.CCRecipient, actual[1].ParticipationTypeMaskEnum);
		}

		[TestMethod]
		public void GetActivityPartiesForApprovalLevel3AwayTest()
		{
			var salesRepId = new Guid(SalesRepId);
			var awayUserId = new Guid(Approver3Id);
			var target = new pnl_appraisal
			{
				OwnerId = new EntityReference("systemuser", salesRepId),
				pnl_RecommendedTradePrice = new Money(117459m)
			};
			var mockService = new Mock<IOrganizationService> { CallBase = false };
			var mockContext = GetMockServiceContext(mockService.Object);
			var mockUserExtensions = new Mock<IUserExtensions> { CallBase = true };
			mockUserExtensions.Setup(u => u.FetchSystemUserById(It.Is<Guid>(p => p == awayUserId)))
				.Returns((Guid id) => new SystemUser { pnl_isaway = true, Id = id });
			mockUserExtensions.Setup(u => u.FetchSystemUserById(It.Is<Guid>(p => p != awayUserId)))
				.Returns((Guid id) => new SystemUser { pnl_isaway = false, Id = id });

			SystemUserExtensions.UserFactory = context => mockUserExtensions.Object; // Replace the factory with one that returns the mock.

			var actual = target.GetActivityPartiesForApproval(mockContext.Object).ToList();

			Assert.AreEqual(3, actual.Count);
			Assert.AreEqual("Richard Wilson", mockContext.Object.SystemUserSet.Single(u => u.Id == actual[0].PartyId.Id).FullName);
			Assert.AreEqual(activityparty_participationtypemask.ToRecipient, actual[0].ParticipationTypeMaskEnum);
			Assert.AreEqual("Peter Marshall", mockContext.Object.SystemUserSet.Single(u => u.Id == actual[1].PartyId.Id).FullName);
			Assert.AreEqual(activityparty_participationtypemask.CCRecipient, actual[1].ParticipationTypeMaskEnum);
			Assert.AreEqual("Roger Nehoff", mockContext.Object.SystemUserSet.Single(u => u.Id == actual[2].PartyId.Id).FullName);
			Assert.AreEqual(activityparty_participationtypemask.CCRecipient, actual[2].ParticipationTypeMaskEnum);
		}

		[TestMethod]
		public void GetActivityPartiesForApprovalLevel4Test()
		{
			var salesRepId = new Guid(SalesRepId);
			var target = new pnl_appraisal
			{
				OwnerId = new EntityReference("systemuser", salesRepId),
				pnl_RecommendedTradePrice = new Money(184459m)
			};
			var mockService = new Mock<IOrganizationService> { CallBase = false };
			var mockContext = GetMockServiceContext(mockService.Object);
			var xrmContext = mockContext.Object;
			// Reset the user factory back since the away tests run first and mock it.
			SystemUserExtensions.UserFactory = context => new UserExtensions(mockContext.Object); 
			
			var actual = target.GetActivityPartiesForApproval(xrmContext).ToList();

			Assert.AreEqual(3, actual.Count);
			Assert.AreEqual("Richard Wilson", actual[0].PartyId.Name);
			Assert.AreEqual(activityparty_participationtypemask.ToRecipient, actual[0].ParticipationTypeMaskEnum);
			Assert.AreEqual("Peter Marshall", actual[1].PartyId.Name);
			Assert.AreEqual(activityparty_participationtypemask.CCRecipient, actual[1].ParticipationTypeMaskEnum);
			Assert.AreEqual("Roger Nehoff", actual[2].PartyId.Name);
			Assert.AreEqual(activityparty_participationtypemask.CCRecipient, actual[1].ParticipationTypeMaskEnum);
		}

		[TestMethod]
		public void GetActivityPartiesForApprovalLevel4AwayTest()
		{
			var salesRepId = new Guid(SalesRepId);
			var awayUserId = new Guid(Approver4Id);
			var target = new pnl_appraisal
			{
				OwnerId = new EntityReference("systemuser", salesRepId),
				pnl_RecommendedTradePrice = new Money(184459m)
			};
			var mockService = new Mock<IOrganizationService> { CallBase = false };
			var mockContext = GetMockServiceContext(mockService.Object);
			var xrmContext = mockContext.Object;
			var mockUserExtensions = new Mock<IUserExtensions> { CallBase = true };
			mockUserExtensions.Setup(u => u.FetchSystemUserById(It.Is<Guid>(p => p == awayUserId)))
				.Returns((Guid id) => new SystemUser { pnl_isaway = true, Id = id });
			mockUserExtensions.Setup(u => u.FetchSystemUserById(It.Is<Guid>(p => p != awayUserId)))
				.Returns((Guid id) => new SystemUser { pnl_isaway = false, Id = id });

			SystemUserExtensions.UserFactory = context => mockUserExtensions.Object; // Replace the factory with one that returns the mock.

			var actual = target.GetActivityPartiesForApproval(xrmContext).ToList();

			// Note, it hasn't really been determined what the plug-in will do in the case where there is no-one to send the email to,
			// since Richard is away. Business logic could error or send to an admin user.
			Assert.AreEqual(3, actual.Count);
			Assert.AreEqual("Peter Marshall", xrmContext.SystemUserSet.Single(u => u.Id == actual[0].PartyId.Id).FullName);
			Assert.AreEqual(activityparty_participationtypemask.CCRecipient, actual[0].ParticipationTypeMaskEnum);
			Assert.AreEqual("Roger Nehoff", xrmContext.SystemUserSet.Single(u => u.Id == actual[1].PartyId.Id).FullName);
			Assert.AreEqual(activityparty_participationtypemask.CCRecipient, actual[1].ParticipationTypeMaskEnum);
			Assert.AreEqual("Richard Wilson", xrmContext.SystemUserSet.Single(u => u.Id == actual[2].PartyId.Id).FullName);
			Assert.AreEqual(activityparty_participationtypemask.CCRecipient, actual[2].ParticipationTypeMaskEnum);
		}

		private IQueryable<pnl_delegatedfinancialauthority> GetFakeDelegatedFinancialAuthorities()
		{
			var entity = new Entity("pnl_delegatedfinancialauthority");
			entity.Attributes.Add("pnl_level1approverid", new EntityReference("systemuser", new Guid(Approver1Id)));
			entity.Attributes.Add("pnl_level2approverid", new EntityReference("systemuser", new Guid(Approver2Id)));
			entity.Attributes.Add("pnl_level3approverid", new EntityReference("systemuser", new Guid(Approver3Id)));
			entity.Attributes.Add("pnl_level4approverid", new EntityReference("systemuser", new Guid(Approver4Id)));
			entity.Attributes.Add("pnl_salesrepid", new EntityReference("systemuser", new Guid(SalesRepId)));
			entity.Attributes.Add("pnl_emailto", new EntityReference("systemuser", new Guid(Approver1Id)));
			entity.Attributes.Add("statecode", new OptionSetValue(0));
			entity.Attributes.Add("pnl_type", new OptionSetValue((int)pnl_delegatedfinancialauthoritytype.Tradein));

			return new List<pnl_delegatedfinancialauthority>
			{
				entity.ToEntity<pnl_delegatedfinancialauthority>()
			}.AsQueryable();
		}

		private IQueryable<pnl_delegatedfinancialauthoritylevel> GetFakeDelegatedFinancialAuthorityLevels()
		{
			var level1 = GetFakeDelegatedFinancialAuthorityLevel(50000m, "1", _comparative2Id);
			var level2 = GetFakeDelegatedFinancialAuthorityLevel(100000m, "2", _comparative2Id);
			var level3 = GetFakeDelegatedFinancialAuthorityLevel(150000m, "3", _comparative2Id);
			var level4 = GetFakeDelegatedFinancialAuthorityLevel(0m, "4", _comparative1Id);

			return new List<pnl_delegatedfinancialauthoritylevel> {level1, level2, level3, level4}.AsQueryable();
		}

		internal pnl_delegatedfinancialauthoritylevel GetFakeDelegatedFinancialAuthorityLevel(decimal amount, string level, Guid comparativeOperatorId)
		{
			var entity = new Entity("pnl_delegatedfinancialauthoritylevel") {Id = Guid.Empty};
			entity.Attributes.Add("pnl_amount", amount);
			entity.Attributes.Add("pnl_comparativeid", new EntityReference("pnl_comparativeoperator", comparativeOperatorId));
			entity.Attributes.Add("pnl_level", level);
			entity.Attributes.Add("pnl_type", new OptionSetValue((int)pnl_delegatedfinancialauthoritytype.Tradein));
			entity.Attributes.Add("statecode", new OptionSetValue(0));

			return entity.ToEntity<pnl_delegatedfinancialauthoritylevel>();
		}

		private IQueryable<SystemUser> GetFakeSystemUsers()
		{
			var approver1 = new Entity("systemuser") {Id = new Guid(Approver1Id)};
			approver1.Attributes.Add("systemuserid", new Guid(Approver1Id));
			approver1.Attributes.Add("fullname", "Peter Marshall");
			approver1.Attributes.Add("isdisabled", false);
			approver1.Attributes.Add("pnl_isaway", false);

			var approver2 = new Entity("systemuser") {Id = new Guid(Approver2Id)};
			approver2.Attributes.Add("systemuserid", new Guid(Approver2Id));
			approver2.Attributes.Add("fullname", "Roger Nehoff");
			approver2.Attributes.Add("isdisabled", false);
			approver2.Attributes.Add("pnl_isaway", false);

			var approver4 = new Entity("systemuser") { Id = new Guid(Approver4Id) };
			approver4.Attributes.Add("systemuserid", new Guid(Approver4Id));
			approver4.Attributes.Add("fullname", "Richard Wilson");
			approver4.Attributes.Add("isdisabled", false);
			approver4.Attributes.Add("pnl_isaway", false);

			return new List<SystemUser> {approver1.ToEntity<SystemUser>(), approver2.ToEntity<SystemUser>(), approver4.ToEntity<SystemUser>()}.AsQueryable();
		}

		internal IQueryable<pnl_comparativeoperator> GetFakeComparativeOperators()
		{
			var operator1 = new Entity("pnl_comparativeoperator") {Id = _comparative1Id};
			operator1.Attributes.Add("pnl_symbol", ">");
			operator1.Attributes.Add("statecode", new OptionSetValue(0));

			var operator2 = new Entity("pnl_comparativeoperator") { Id = _comparative2Id };
			operator2.Attributes.Add("pnl_symbol", "<=");
			operator2.Attributes.Add("statecode", new OptionSetValue(0));

			return
				new List<pnl_comparativeoperator>
				{
					operator1.ToEntity<pnl_comparativeoperator>(),
					operator2.ToEntity<pnl_comparativeoperator>()
				}.AsQueryable();
		}

		internal Mock<CrmServiceContext> GetMockServiceContext(IOrganizationService organizationService)
		{
			var mockContext = new Mock<CrmServiceContext>(organizationService);
			mockContext.Setup(c => c.pnl_delegatedfinancialauthoritySet).Returns(GetFakeDelegatedFinancialAuthorities);
			mockContext.Setup(c => c.pnl_delegatedfinancialauthoritylevelSet).Returns(GetFakeDelegatedFinancialAuthorityLevels);
			mockContext.Setup(c => c.pnl_comparativeoperatorSet).Returns(GetFakeComparativeOperators);
			mockContext.Setup(c => c.SystemUserSet).Returns(GetFakeSystemUsers);

			return mockContext;
		}

		[TestMethod]
		public void GetExpectedRetainedMarginDollarsNullTest()
		{
			var target = new pnl_appraisal();

			var actual = target.GetExpectedRetainedMarginDollars();

			Assert.AreEqual(0, actual);
		}

		[TestMethod]
		public void GetExpectedRetainedMarginDollarsNullPercentageTest()
		{
			var target = new pnl_appraisal
			{
				pnl_RecommededListPrice = new Money(25649m)
			};

			var actual = target.GetExpectedRetainedMarginDollars();

			Assert.AreEqual(0, actual);
		}

		[TestMethod]
		public void GetExpectedRetainedMarginDollarsTest()
		{
			var target = new pnl_appraisal
			{
				pnl_RecommededListPrice = new Money(31999m),
				pnl_ExpectedRetainedMarginPercentage = 10m
			};

			var actual = target.GetExpectedRetainedMarginDollars();

			Assert.AreEqual(3199.9m, actual);
		}

		[TestMethod]
		public void GetCalculatedTradePriceNullTest()
		{
			var target = new pnl_appraisal();

			var actual = target.GetCalculatedTradePrice(0m);

			Assert.AreEqual(0, actual);
		}

		[TestMethod]
		public void GetTotalCostTest()
		{
			var target = new pnl_appraisal
			{
				pnl_RepairCosts = new Money(879m),
				pnl_Freight = new Money(249m),
				pnl_PreDeliveryExpenses = new Money(446m),
				pnl_OtherCosts = new Money(369m)
			};

			var actual = target.GetTotalCost();

			Assert.AreEqual(1943m, actual);
		}

		[TestMethod]
		public void GetCalculatedTradePriceTest()
		{
			var target = new pnl_appraisal
			{
				pnl_RecommededListPrice = new Money(31999m),
				pnl_ExpectedRetainedMarginPercentage = 10m,
				pnl_RepairCosts = new Money(879m),
				pnl_Freight = new Money(249m),
				pnl_PreDeliveryExpenses = new Money(446m),
				pnl_OtherCosts = new Money(369m)
			};

			// Average Valuation - Total Cost - Expected Retained Margin $ = 32,542 - 1,943 - 3,199.9 = 27,399.1
			var actual = target.GetCalculatedTradePrice(32542m);

			Assert.AreEqual(27399.1m, actual);
		}
	}
}
