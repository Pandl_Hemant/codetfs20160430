﻿using LandPower;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;

namespace Plugins7_QuoteToOrder
{
    public class QuoteWon : IPlugin
    {
        IOrganizationService _service;

        public void Execute(IServiceProvider serviceProvider)
        {
            
            ITracingService tracingService =
                (ITracingService)serviceProvider.GetService(typeof(ITracingService));            
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            
            _service = serviceFactory.CreateOrganizationService(context.UserId);

            if (context.Depth > 1)
                return;

            crmServiceContext _ctx = new LandPower.crmServiceContext(_service);

            if(context.InputParameters.Contains("QuoteClose") && context.InputParameters["QuoteClose"] is Entity)
            {
                
                Entity qClose = (Entity)context.InputParameters["QuoteClose"];
                EntityReference quoteEntity = (EntityReference)qClose.Attributes["quoteid"];
                var qte = (from q in _ctx.QuoteSet
                            where q.Id.Equals(quoteEntity.Id)
                            select q).FirstOrDefault();               
                    var qMachines = from qm in _ctx.pnl_quotemachineSet
                                    where qm.pnl_Quoteid.Id.Equals(quoteEntity.Id)
                                    select qm;
                    foreach (pnl_quotemachine qmac in qMachines)
                    {
                        if (qmac.pnl_Configurationid == null)
                        {
                            throw new InvalidPluginExecutionException("Please make sure all Trade-in Machines have a value in the Configuration column");
                        }
                        var machine = (from m in _ctx.pnl_machineSet
                                      where m.Id.Equals(qmac.pnl_Machineid.Id)
                                      select m).FirstOrDefault();

                        if (machine.pnl_CurrentCustomerid.Id != qte.CustomerId.Id)
                        {
                            throw new InvalidPluginExecutionException("Machine - " +machine.pnl_name +" is owned by different Customer");
                        }
                        
                        if (machine.statecode.Value ==  pnl_machineState.Inactive)
                        { 
                            throw new InvalidPluginExecutionException("Machine - " + machine.pnl_name +" is already sold");
                        }

                        var app = (from a in _ctx.pnl_appraisalSet
                                   where a.pnl_Machineid.Id.Equals(machine.Id)
                                   orderby a.pnl_appraisaldate  descending
                                   select a).FirstOrDefault();
                        if (app != null)
                        {
                            if (app.pnl_ValuationApprovedOn < DateTime.Now.AddMonths(-3))
                            {
                                throw new InvalidPluginExecutionException("Machine - " + machine.pnl_name + " has expired");
                            }
                            if (app.statuscode != null && app.statuscode.Value.ToString() != "125760001")
                            {
                                throw new InvalidPluginExecutionException("Appraisal attached to machine - " + machine.pnl_name + " is not approved");
                            }
                        }
                        var buCustmer = (from cu in _ctx.BusinessUnitSet
                                         where cu.Id.Equals(qte.OwningBusinessUnit)
                                         select cu).FirstOrDefault();
                        if (buCustmer.pnl_customerid == null)
                        {
                            throw new InvalidPluginExecutionException("There is no Dealer - Customer record assigned to your Bussiness Unit. Please contact your System Administrator to resolve this issue.");
                        }
                        else
                        {
                            pnl_machine updateMachineCustomer = new pnl_machine();
                            updateMachineCustomer.Id = machine.Id;
                            updateMachineCustomer.pnl_CurrentCustomerid = (EntityReference)buCustmer.pnl_customerid;
                            _service.Update(updateMachineCustomer);

                        }                     

                    }

                    
                    ColumnSet salesOrderColumns = new ColumnSet("salesorderid", "totalamount");

                    // Convert the quote to a sales order
                    ConvertQuoteToSalesOrderRequest convertQuoteRequest =
                        new ConvertQuoteToSalesOrderRequest()
                        {
                            QuoteId = qte.Id,
                            ColumnSet = salesOrderColumns
                        };

                    SalesOrderDetail asd = new SalesOrderDetail();
                

                    ConvertQuoteToSalesOrderResponse convertQuoteResponse =
                        (ConvertQuoteToSalesOrderResponse)_service.Execute(convertQuoteRequest);
                    SalesOrder salesOrder = (SalesOrder)convertQuoteResponse.Entity;
                    foreach (pnl_quotemachine qmac in qMachines)
                    {
                        pnl_quotemachine updateQuoteMachine = new pnl_quotemachine();
                        updateQuoteMachine.Id = qmac.Id;
                        updateQuoteMachine.pnl_Orderid = salesOrder.ToEntityReference();
                        _service.Update(updateQuoteMachine);
                    }
                    var lsourcedmachine = from lsm in _ctx.pnl_locallysourcedmachineSet
                                          where lsm.pnl_QuoteId.Equals(qte.Id)
                                          select lsm;
                    foreach (pnl_locallysourcedmachine ls in lsourcedmachine)
                    {
                        pnl_locallysourcedmachine updatelsMachine = new pnl_locallysourcedmachine();
                        updatelsMachine.Id = ls.Id;
                        updatelsMachine.pnl_Orderid = salesOrder.ToEntityReference();
                        _service.Update(updatelsMachine);
                    }
                    var lsourceditem = from lsi in _ctx.pnl_locallysourceditemSet
                                       where lsi.pnl_Quote.Equals(qte.Id)
                                       select lsi;
                    foreach (pnl_locallysourceditem lsitems in lsourceditem)
                    {
                        pnl_locallysourceditem updatelsitems = new pnl_locallysourceditem();
                        updatelsitems.Id = lsitems.Id;
                        updatelsitems.pnl_Orderid = salesOrder.ToEntityReference();
                        _service.Update(updatelsitems);
                    }
                    //throw new InvalidPluginExecutionException("Machine -  has expired"); 
                   
            }   
        }
    }
}
