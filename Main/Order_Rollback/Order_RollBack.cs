﻿using LandPower;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;

namespace Order_Rollback
{
    public class Order_RollBack : IPlugin
    {
        IOrganizationService _service;

        public void Execute(IServiceProvider serviceProvider)
        {

            ITracingService tracingService =
                (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));

            _service = serviceFactory.CreateOrganizationService(context.UserId);
            Entity postEntity = (Entity)context.PostEntityImages["entity"];
            if (context.Depth > 1)
                return;
            crmServiceContext _ctx = new LandPower.crmServiceContext(_service);
            

            if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
            {
                Entity orderEntity = (Entity)context.InputParameters["Target"];
                
                EntityReference relatedQuote = (EntityReference)postEntity.Attributes["quoteid"];
                if (orderEntity.Contains("pnl_rollbackdate") && orderEntity.Attributes["pnl_rollbackdate"] != null)
                {
                    SetStateRequest qdraft = new SetStateRequest();
                    qdraft.State = new OptionSetValue((int)QuoteState.Draft);
                    qdraft.Status = new OptionSetValue((int)1);
                    qdraft.EntityMoniker = relatedQuote;
                    _service.Execute(qdraft);

                    //set orderid for quotemachines linked to this order to null
                    var qmachines = from o in _ctx.pnl_quotemachineSet
                                    where o.pnl_Orderid.Id.Equals(orderEntity.Id)
                                    select o;
                    foreach (pnl_quotemachine qm in qmachines)
                    {

                        var updateqm = new pnl_quotemachine
                        {
                            Id = qm.Id,
                            pnl_Orderid = null
                        };
                        _service.Update(updateqm);
                    }

                    //set orderid for locally sourced items linked to this order to null
                    var lsi = from ls in _ctx.pnl_locallysourceditemSet
                              where ls.pnl_Orderid.Id.Equals(orderEntity.Id)
                              select ls;
                    foreach (pnl_locallysourceditem l in lsi)
                    {
                        var updatel = new pnl_locallysourceditem
                        {
                            Id = l.Id,
                            pnl_Orderid = null
                        };
                        _service.Update(updatel);
                    }
                    //set orderid for locally sourced machines linked to this order to null
                    var lsm = from lm in _ctx.pnl_locallysourcedmachineSet
                              where lm.pnl_Orderid.Id.Equals(orderEntity.Id)
                              select lm;
                    foreach (pnl_locallysourcedmachine m in lsm)
                    {
                        var updatelsm = new pnl_locallysourcedmachine
                        {
                            Id = m.Id,
                            pnl_Orderid = null
                        };
                        _service.Update(updatelsm);
                    }
                    Guid _closeSalesOrderId = postEntity.Id;
                    CancelSalesOrderRequest cancelRequest = new CancelSalesOrderRequest()
                    {
                        OrderClose = new OrderClose()
                        {
                            SalesOrderId = postEntity.ToEntityReference(),
                            Subject = "Close Sales Order " + DateTime.Now
                        },
                        Status = new OptionSetValue(-1)
                    };
                    _service.Execute(cancelRequest);

                }
               
            }

        }
    }
}
