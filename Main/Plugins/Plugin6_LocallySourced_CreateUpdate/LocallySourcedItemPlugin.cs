﻿using System;

using Microsoft.Xrm.Sdk;

namespace LandPowerPlugins
{
    public class LocallySourcedItemPlugin : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            //var tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            var context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            if (context.Depth > 1)
                return;

            if (context.IsOfflinePlayback) // Don't redo create logic when synchronizing back online.
                return;

            // Update the parent quote's lastsavedfromlocallysourceditem field on create or update of a locally sourced item.
            if ((context.MessageName.ToUpper() == "CREATE" || context.MessageName.ToUpper() == "UPDATE") && context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
            {
                try
                {
                    var serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                    var service = serviceFactory.CreateOrganizationService(context.InitiatingUserId);

                    var entity = (Entity)context.InputParameters["Target"];

                    if (entity.LogicalName == "pnl_locallysourceditem")
                    {
                        if (context.PostEntityImages.Contains("postimage") && (context.PostEntityImages["postimage"] != null))
                        {
                            var locallysourced = context.PostEntityImages["postimage"];

                            if(locallysourced.Attributes.Contains("pnl_quote")){
                                var quote = new Entity("quote")
                                {
                                    Id = ((EntityReference) locallysourced.Attributes["pnl_quote"]).Id
                                };
                                quote["pnl_lastsavedfromlocallysourceditemon"] = DateTime.Now;
                                service.Update(quote);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new InvalidPluginExecutionException(string.Format("LocallySourcedItemPlugin exception: {0}, {1}, {2}", ex.Message, ex.InnerException, ex.StackTrace));
                }
            }

            //DELETE
            if (context.MessageName.ToUpper() == "DELETE" && context.InputParameters.Contains("Target") && context.InputParameters["Target"] is EntityReference)
            {
                try
                {
                    var serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                    var service = serviceFactory.CreateOrganizationService(context.InitiatingUserId);

                    var entity = (EntityReference)context.InputParameters["Target"];

                    if (entity.LogicalName != "pnl_locallysourceditem") return;
                    if (!context.PreEntityImages.Contains("preimage") || (context.PreEntityImages["preimage"] == null)) return;
                    
                    var locallysourced = context.PreEntityImages["preimage"];

                    if (!locallysourced.Attributes.Contains("pnl_quote")) return;
                            
                    var quote = new Entity("quote")
                    {
                        Id = ((EntityReference) locallysourced.Attributes["pnl_quote"]).Id
                    };
                    quote["pnl_lastsavedfromlocallysourceditemon"] = DateTime.Now;
                    service.Update(quote);
                }
                catch (Exception ex)
                {
                    throw new InvalidPluginExecutionException(string.Format("LocallySourcedItemPlugin exception: {0}, {1}, {2}", ex.Message, ex.InnerException, ex.StackTrace));
                }
            }
        }
    }
}