﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace LandPowerPlugins.Extensions
{


    public static class AppraisalItemExtension
    {
        public static decimal GetItemValue(this pnl_appraisalitem appraisalitem)
        {
            if ((null != appraisalitem.pnl_FieldType &&
                (appraisalitem.pnl_FieldType.Value == (int)pnl_appraisalitemfieldtype.Memo ||
                 appraisalitem.pnl_FieldType.Value == (int)pnl_appraisalitemfieldtype.Picklist)) || appraisalitem.pnl_Value == null)
            {
                return 0;
            }
            decimal value;
            decimal.TryParse(appraisalitem.pnl_Value.Replace("$", "").Replace(",", ""), out value);

            return Math.Round(value, 0, MidpointRounding.AwayFromZero);
        }
    }
}
