﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LandPowerPlugins.Extensions;

namespace LandPowerPlugins
{
    class AppraisalCostAndValuationCalculation
    {

        #region Properties

        public CrmServiceContext Context { get; set; }

        #endregion

        public Decimal GetCalculatedAverage(Guid appraisalId)
        {
            var items = (from a in Context.pnl_appraisalitemSet
                         join g in Context.pnl_appraisalitemgroupSet on a.pnl_AppraisalItemGroupId.Id equals g.Id
                         join gn in Context.pnl_groupnameSet on g.pnl_GroupNameId.Id equals gn.Id
                         join n in Context.pnl_appraisalitemnameSet on a.pnl_AppraisalItemNameId.Id equals n.Id
                         where a.pnl_AppraisalId.Id == appraisalId
                               && a.statecode == pnl_appraisalitemState.Active
                               && gn.pnl_name == "Valuation"
                               && (a.pnl_CalcCode == "SOURCE_1" || a.pnl_CalcCode == "SOURCE_2" || a.pnl_CalcCode == "SOURCE_3" || a.pnl_CalcCode == "SOURCE_4")
                         select a);

            var totalValue = 0m;
            var itemCount = 0;

            foreach (var item in items)
            {
                var itemValue = item.GetItemValue();
                if (itemValue == 0m) continue;
                itemCount++;
                totalValue += itemValue;
            }

            var calcAverage = itemCount > 0 ? Math.Round(totalValue / itemCount, 0, MidpointRounding.AwayFromZero) : 0m;

            return calcAverage;
        }

        public Decimal GetTotalCosts(pnl_appraisal appraisal)
        {
            var totalCost = appraisal.pnl_OtherCosts == null ? 0m : Math.Round(appraisal.pnl_OtherCosts.Value, 0, MidpointRounding.AwayFromZero);
            totalCost += (appraisal.pnl_RepairCosts == null ? 0m : Math.Round(appraisal.pnl_RepairCosts.Value, 0, MidpointRounding.AwayFromZero));
            totalCost += (appraisal.pnl_Freight == null ? 0m : Math.Round(appraisal.pnl_Freight.Value, 0, MidpointRounding.AwayFromZero));
            totalCost += (appraisal.pnl_PreDeliveryExpenses == null ? 0m : Math.Round(appraisal.pnl_PreDeliveryExpenses.Value, 0, MidpointRounding.AwayFromZero));

            return totalCost;
        }

        public Decimal GetExpectedRetainedMargin(pnl_appraisal appraisal)
        {
            if (appraisal.pnl_RecommededListPrice == null) return 0m;

            return Math.Round((appraisal.pnl_RecommededListPrice.Value * appraisal.pnl_ExpectedRetainedMarginPercentage.GetValueOrDefault()) /
                   100m, 0, MidpointRounding.AwayFromZero);
        }

        public Decimal  GetCalculatedTradePrice(pnl_appraisal appraisal)
        {
            //need to look into below for efficiency -- old plugin was doing this, guess it's ok>?
            var calculatedTradePrice = (appraisal.pnl_RecommededListPrice == null
                                           ? 0m
                                           : Math.Round(appraisal.pnl_RecommededListPrice.Value, 0, MidpointRounding.AwayFromZero)) - GetTotalCosts(appraisal) - GetExpectedRetainedMargin(appraisal);

            return calculatedTradePrice;
        }

    }
}
