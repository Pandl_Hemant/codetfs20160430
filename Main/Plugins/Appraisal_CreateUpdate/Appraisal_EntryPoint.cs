using System;
using System.Linq;
using System.ServiceModel;
using Microsoft.Xrm.Sdk;

using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using System.Data.SqlTypes;
using System.Text;
using System.Collections.Generic;


namespace LandPowerPlugins
{
    public class Appraisal_EntryPoint : IPlugin
    {
        #region Private Vars

        IOrganizationService _service;
        CrmServiceContext _ctx;
        Entity _updateEntity;

        #endregion

        #region Public Methods

        public void Execute(IServiceProvider serviceProvider)
        {
            //Extract the tracing service for use in debugging sandboxed plug-ins.
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            // Obtain the execution context from the service provider.
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            // don't want to redo create logic when resyncing online
            if (context.IsOfflinePlayback)
                return;

            // The InputParameters collection contains all the data passed in the message request.
            if (!context.InputParameters.Contains("Target") || !(context.InputParameters["Target"] is Entity))
                return;

            if (context.Depth > 3) return;

            // Obtain the target entity from the input parameters.
            _updateEntity = (Entity)context.InputParameters["Target"];

            // Verify that the target entity represents an appraisal.
            // If not, this plug-in was not registered correctly.
            if (_updateEntity.LogicalName != "pnl_appraisal")
                return;

            ExecuteMultipleRequest requestWithContinueOnError = 
                new ExecuteMultipleRequest() { Settings = new ExecuteMultipleSettings() { ContinueOnError = false, ReturnResponses = true }, Requests = new OrganizationRequestCollection() };

            try
            {
                // Obtain the organization service reference.
                IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                _service = serviceFactory.CreateOrganizationService(context.UserId);

                // Use reference to invoke pre-built types service 
                _ctx = new CrmServiceContext(_service);

                Entity appraisalPreImageEntity = null;
                if (context.PreEntityImages.Contains("PreImage") && (context.PreEntityImages["PreImage"] is Entity))
                {
                    appraisalPreImageEntity = (Entity)context.PreEntityImages["PreImage"];
                }
                if (appraisalPreImageEntity == null || appraisalPreImageEntity.LogicalName != "pnl_appraisal")
                {
                    throw new InvalidPluginExecutionException("Incorrect Plugin registration: a Pre image has not been correctly configured for this plugin.");
                }

                pnl_appraisal appraisalUpdate = _updateEntity.ToEntity<pnl_appraisal>();
                pnl_appraisal appraisalPreImage = appraisalPreImageEntity.ToEntity<pnl_appraisal>();

                //this status or preImage status?
                var status =_updateEntity.GetAttributeValue<OptionSetValue>("statuscode");

                //this correct or check against other statuses? Also should it return or except?
                if (status.Value != (int)pnl_appraisal_statuscode.Inspection_1) return;

                if (appraisalUpdate.pnl_syncstartedon.HasValue && appraisalUpdate.pnl_syncendedon.HasValue)
                {
                    if (appraisalUpdate.pnl_syncstartedon.Value > appraisalUpdate.pnl_syncendedon && appraisalUpdate.pnl_syncendedon.Value <= DateTime.Now.Subtract(new TimeSpan(0, 60, 0))
                    {
                        return;
                    }
                }
                else
                {
                    //not sure if should return or just acknowledge that this isn't a iTrade update
                }

                //******* am assuming will need to trap the messages like this somehow?
                //******* also that's the correct property to use
                if (context.MessageName == "Update")
                {
                    //do updates
                }
                else if (context.MessageName == "Create")
                {
                    //inserts
                }
                else if (context.MessageName == "Delete")
                {
                    //deletes
                }

                

                //var renamePlugin = new AppraisalRename();
                //renamePlugin.Context = _ctx;
                //var updatedName = renamePlugin.UpdateAppraisalName(appraisalPreImage);
                //if (updatedName == appraisalPreImage.pnl_name) return;
                
                //var testString = String.Format("{0}.{1}", "THIS IS A TEST ***", DateTime.Now.ToLocalTime().ToShortDateString());
                //appraisalUpdate.pnl_name = testString;
                //_service.Update(appraisalUpdate);
                //renamePlugin.UpdateChildItems(appraisalPreImage.Id, testString, _service);
                ////appraisalUpdate.pnl_name = updatedName;
                //var calculations = new AppraisalCostAndValuationCalculation();
                //var avg = calculations.GetCalculatedAverage(appraisalPreImage.Id);
                //var erm = calculations.GetExpectedRetainedMargin(appraisalPreImage);
                //var ctp = calculations.GetCalculatedTradePrice(appraisalPreImage);
                //var tc = calculations.GetTotalCosts(appraisalPreImage);

                //appraisalUpdate.pnl_TotalCosts = new Money(tc);
                //appraisalUpdate.pnl_CalculatedAverage = new Money(avg);
                //appraisalUpdate.pnl_ExpectedRetainedMarginDollar = new Money(erm);
                //appraisalUpdate.pnl_CalcuatedTradePrice = new Money(ctp);
                //_service.Update(appraisalUpdate);
                //// todo: add approval code

                ////appraisal edit code
                //Entity postImageEntity = (context.PostEntityImages != null && context.PostEntityImages.Contains("PostImage")) ? context.PostEntityImages["PostImage"] : null;


            }
            #region catch - Exception handling
            catch (FaultException<OrganizationServiceFault> ex)
            {
               throw new InvalidPluginExecutionException(string.Format("An error occurred in the Appraisal Update plug-in:-{0}{1}", Environment.NewLine, ex.Message) );
            }
            #endregion
        }

        #endregion

        #region Private Methods

        

        #endregion

    }
}
