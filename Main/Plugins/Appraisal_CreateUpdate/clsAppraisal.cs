﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LandPowerPlugins
{
    class Appraisal
    {
        public Appraisal(IOrganizationService service, IPluginExecutionContext context, CrmServiceContext crmContext, Entity preImageEntity, Entity updateEntity)
        {
            _service = service;
            _context = context;
            _crmContext = crmContext;
            _preImageEntity = preImageEntity;
            _inputEntity = updateEntity;
        }


        #region Private Vars

        IOrganizationService _service;
        private Entity _preImageEntity;
        private Entity _inputEntity;
        IPluginExecutionContext _context;
        CrmServiceContext _crmContext;

        #endregion
    }
}
