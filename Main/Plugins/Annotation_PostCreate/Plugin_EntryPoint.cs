﻿using LandPowerPlugins;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

using LandPowerPlugins.Extensions;

namespace Annotation_PostCreate
{
    public class Plugin_EntryPoint : IPlugin
    {
        #region Private Vars

        IOrganizationService _service;
        CrmServiceContext _ctx;
        Entity _updateEntity;
        EntityReference _objectId;

        #endregion

        #region Public Methods

        public void Execute(IServiceProvider serviceProvider)
        {
            //Extract the tracing service for use in debugging sandboxed plug-ins.
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            // Obtain the execution context from the service provider.
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            // don't want to redo create logic when resyncing online
            if (context.IsOfflinePlayback)
                return;

            // The InputParameters collection contains all the data passed in the message request.
            if (!context.InputParameters.Contains("Target") || !(context.InputParameters["Target"] is Entity))
                return;

            if (context.Depth > 3) return;

            // Obtain the target entity from the input parameters.
            _updateEntity = (Entity)context.InputParameters["Target"];
            var preImageEnitity = (context.PreEntityImages != null && context.PreEntityImages.Contains("PreImage")) ? context.PreEntityImages["PreImage"] : null;

            ExecuteMultipleRequest requestWithContinueOnError =
                new ExecuteMultipleRequest() { Settings = new ExecuteMultipleSettings() { ContinueOnError = false, ReturnResponses = true }, Requests = new OrganizationRequestCollection() };

            try
            {
                // Obtain the organization service reference.
                IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                _service = serviceFactory.CreateOrganizationService(context.UserId);
                // Use reference to invoke pre-built types service 
                _ctx = new CrmServiceContext(_service);

                var annotation = _updateEntity.ToEntity<Annotation>();
                _objectId = annotation.ObjectId;
                if (preImageEnitity != null && _objectId == null)
                {
                    var preNote = preImageEnitity.ToEntity<Annotation>();
                    _objectId = preNote.ObjectId;
                }

                if (!string.IsNullOrEmpty(annotation.DocumentBody))
                {
                    CheckAppraisalIsInInspection(annotation);
                    CheckDuplicateAttachments(annotation);
                }

            }
            #region catch - Exception handling
            catch (FaultException<OrganizationServiceFault> ex)
            {
                throw new InvalidPluginExecutionException(string.Format("An error occurred in the Appraisal Update plug-in:-{0}{1}", Environment.NewLine, ex.Message));
            }
            #endregion
        }

        #endregion

        internal void CheckAppraisalIsInInspection(Annotation note)
        {
            if (_objectId == null || _objectId.LogicalName.ToLower() != "pnl_appraisalphoto") return;

            var appraisalPhoto = _ctx.AppraisalPhoto().FetchAppraisalPhotoById(_objectId.Id);

            if (appraisalPhoto == null) throw new InvalidPluginExecutionException("Unable to retrieve AppraisalPhoto.");

            var appraisalId = appraisalPhoto.pnl_AppraisalId == null ? Guid.Empty : appraisalPhoto.pnl_AppraisalId.Id;

            if (appraisalId == Guid.Empty) throw new InvalidPluginExecutionException("Unable to find Appraisal.");

            var appraisal = _ctx.Appraisal().FetchAppraisalById(appraisalId);

            if (appraisal == null) throw new InvalidPluginExecutionException("Unable to retrieve Appraisal.");

            if (!appraisal.CanUploadPhotos(_ctx))
                throw new InvalidPluginExecutionException("Appraisal must be in Inspection to upload Photos.");
        }

        internal void CheckDuplicateAttachments(Annotation note)
        {
            if (_objectId == null || _objectId.LogicalName.ToLower() != "pnl_appraisalphoto") return;

            //var appraisalPhoto = new pnl_appraisalphoto { Id = _objectId.Id };

            var photoCount = _ctx.AppraisalPhoto().GetPhotoCount(_objectId.Id);

            //var photoCount = appraisalPhoto.GetPhotoCount(new CrmServiceContext(OrganizationService));

            if (photoCount > 2)
                throw new InvalidPluginExecutionException("Only one attachment is allowed for an AppraisalPhoto.");
        }
    }
}
