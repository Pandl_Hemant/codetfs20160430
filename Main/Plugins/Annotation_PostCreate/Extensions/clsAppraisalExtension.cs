﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LandPowerPlugins;
using Microsoft.Xrm.Sdk;

namespace LandPowerPlugins.Extensions
{
    public static class AppraisalExtension
    {

        public static bool HaveAllPhotosUploaded(this pnl_appraisal appraisal, CrmServiceContext serviceContext)
        {
            var photoCount = serviceContext.Appraisal().FetchAppraisalPhotoAttachmentCount(appraisal.Id);

            return photoCount == appraisal.pnl_itradephotocount.GetValueOrDefault();
        }

        public static string BuildAppraisalLink(this pnl_appraisal appraisal, string organizationName)
        {
            var random = new Random();
            var histKey = random.Next(100000000, 900000000);
            var url = string.Format("https://{0}.landpower.co.nz/main.aspx?etn=pnl_appraisal&histKey={1}&id={2}&pagetype=entityrecord",
                organizationName, histKey, appraisal.Id);
            url = Uri.EscapeUriString(url);

            return string.Format("<a href=\"{0}\">{1}</a>", url, System.Net.WebUtility.HtmlEncode(appraisal.pnl_name));
        }

        public static bool CanUploadPhotos(this pnl_appraisal appraisal, CrmServiceContext serviceContext)
        {
            return (appraisal.statuscode.Value == (int)pnl_appraisal_statuscode.Inspection_1 ||
                appraisal.statuscode.Value == (int)pnl_appraisal_statuscode.Inspection_2);
        }
    }
}
