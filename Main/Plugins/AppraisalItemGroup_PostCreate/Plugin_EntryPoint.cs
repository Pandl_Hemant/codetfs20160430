﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LandPowerPlugins;
using System.ServiceModel;

namespace AppraisalItemGroup_PostUpdate
{
    class Plugin_EntryPoint : IPlugin
    {

        #region Private Vars

        IOrganizationService _service;
        CrmServiceContext _ctx;
        Entity _updateEntity;

        #endregion

        #region Public Methods

        public void Execute(IServiceProvider serviceProvider)
        {
            //Extract the tracing service for use in debugging sandboxed plug-ins.
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            // Obtain the execution context from the service provider.
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            // don't want to redo create logic when resyncing online
            if (context.IsOfflinePlayback)
                return;

            // The InputParameters collection contains all the data passed in the message request.
            if (!context.InputParameters.Contains("Target") || !(context.InputParameters["Target"] is Entity))
                return;

            if (context.Depth > 3) return;

            // Obtain the target entity from the input parameters.
            _updateEntity = (Entity)context.InputParameters["Target"];

            var postImageEntity = (context.PostEntityImages != null && context.PostEntityImages.Contains("postimage")) ? context.PostEntityImages["postimage"] : null;

            ExecuteMultipleRequest requestWithContinueOnError = 
                new ExecuteMultipleRequest() { Settings = new ExecuteMultipleSettings() { ContinueOnError = false, ReturnResponses = true }, Requests = new OrganizationRequestCollection() };

            try
            {
                // Obtain the organization service reference.
                IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                _service = serviceFactory.CreateOrganizationService(context.UserId);
                // Use reference to invoke pre-built types service 
                _ctx = new CrmServiceContext(_service);

                var groupUpdate = new AppraisalItemGroupUpdate(_service, context, _ctx, postImageEntity, _updateEntity);
                groupUpdate.ExecuteAppraisalItemGroupUpdate();

            }
            #region catch - Exception handling
            catch (FaultException<OrganizationServiceFault> ex)
            {
                throw new InvalidPluginExecutionException(string.Format("An error occurred in the Appraisal Update plug-in:-{0}{1}", Environment.NewLine, ex.Message));
            }
            #endregion
        }

        #endregion
    }
}
