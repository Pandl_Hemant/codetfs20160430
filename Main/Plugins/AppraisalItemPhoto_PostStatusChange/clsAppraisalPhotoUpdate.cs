﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LandPowerPlugins.Extensions;

namespace LandPowerPlugins
{
    class AppraisalPhotoUpdate
    {
        public AppraisalPhotoUpdate(IPluginExecutionContext context, CrmServiceContext crmContext)
        {
            context = _context;
            _crmContext = crmContext;
        }

        public void ExecuteAppraisalItemPhotoUpdate(Guid preImageAppraisalId, Guid updateEntityId)
        {
            CheckCanUpdateAppraisal(preImageAppraisalId);
            if (updateEntityId == preImageAppraisalId) return;
            CheckCanUpdateAppraisal(updateEntityId);
        }

        internal void CheckCanUpdateAppraisal(Guid appraisalId)
        {
            var appraisal = _crmContext.Appraisal().FetchAppraisalById(appraisalId);
            var canUpdate = appraisal.CanApplyChanges(_context.InitiatingUserId, _crmContext);

            if (canUpdate) return;

            throw new InvalidPluginExecutionException("Permission denied. The Appraisal can be updated if it is in inspection or you are an approver and it is waiting for approval.");
        }

        #region Private Vars

        CrmServiceContext _crmContext;
        IPluginExecutionContext _context;

        #endregion
    }
}
