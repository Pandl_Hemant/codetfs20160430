﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LandPowerPlugins;
using Microsoft.Xrm.Sdk;

namespace LandPowerPlugins.Extensions
{
    public static class AppraisalExtension
    {
        public static bool HasRequiredItemValues(this pnl_appraisal appraisal, CrmServiceContext serviceContext)
        {
            var items = serviceContext.Appraisal().FetchAppraisalValuationItems(appraisal.Id);

            return items.All(item => !string.IsNullOrEmpty(item.pnl_Value));
        }

        public static bool HaveAllPhotosUploaded(this pnl_appraisal appraisal, CrmServiceContext serviceContext)
        {
            var photoCount = serviceContext.Appraisal().FetchAppraisalPhotoAttachmentCount(appraisal.Id);

            return photoCount == appraisal.pnl_itradephotocount.GetValueOrDefault();
        }

        public static string BuildAppraisalLink(this pnl_appraisal appraisal, string organizationName)
        {
            var random = new Random();
            var histKey = random.Next(100000000, 900000000);
            var url = string.Format("https://{0}.landpower.co.nz/main.aspx?etn=pnl_appraisal&histKey={1}&id={2}&pagetype=entityrecord",
                organizationName, histKey, appraisal.Id);
            url = Uri.EscapeUriString(url);

            return string.Format("<a href=\"{0}\">{1}</a>", url, System.Net.WebUtility.HtmlEncode(appraisal.pnl_name));
        }

        public static void UpdateAppraisalItemValue(this pnl_appraisal appraisal, string itemName, Money value, IOrganizationService organizationService, string itemGroupName = "Costs")
        {
            using (var xrmContext = new CrmServiceContext(organizationService))
            {
                var item = xrmContext.AppraisalItem().FetchAppraisalItemByNameAndGroupName(appraisal.Id, itemName, itemGroupName);
                if (null == item) return; // Maybe we should create the item in this case?

                var itemValue = item.GetItemValue();
                var newValue = value == null ? 0m : Math.Round(value.Value, 0, MidpointRounding.AwayFromZero);
                if (itemValue == newValue) return;

                var entity = new Entity("pnl_appraisalitem") { Id = item.Id };
                entity.Attributes.Add("pnl_value", newValue.ToString("N0")); // e.g. 61,453 (group separator, 0 decimal places).
                organizationService.Update(entity);
            }
        }

        public static bool CanApplyChanges(this pnl_appraisal appraisal, Guid userId, CrmServiceContext serviceContext)
        {
            var user = new SystemUser { Id = userId };

            if (appraisal.statuscode.Value == (int)pnl_appraisal_statuscode.ApprovedApprasial_125760001 || appraisal.statuscode.Value ==
                    (int)pnl_appraisal_statuscode.ApprovedApprasial_125760003)
            {
                return user.IsInRole("System Administrator", serviceContext);
            }

            if (appraisal.statuscode.Value == (int)pnl_appraisal_statuscode.Inspection_1 ||
                appraisal.statuscode.Value == (int)pnl_appraisal_statuscode.Inspection_2)
            {
                return true;
            }

            return user.IsTeamMember("Appraisal Approvers", serviceContext);
        }

        public static void SetAppraisalItemsReadOnly(this pnl_appraisal appraisal, IOrganizationService organizationService, bool readOnly = true)
        {
            var context = new CrmServiceContext(organizationService);
            var items = context.Appraisal().FetchAppraisalItems(appraisal.Id);

            foreach (var item in items)
            {
                SetAppraisalItemReadOnly(item.Id, organizationService, readOnly);
            }
        }


        internal static void SetAppraisalItemReadOnly(Guid appraisalItemId, IOrganizationService organizationService, bool readOnly = true)
        {
            var entity = new Entity("pnl_appraisalitem") { Id = appraisalItemId };
            entity.Attributes.Add("pnl_isreadonly", readOnly);
            organizationService.Update(entity);
        }

        public static IEnumerable<ActivityParty> GetActivityPartiesForApproval(this pnl_appraisal appraisal, CrmServiceContext serviceContext)
        {
            var approvers = new List<ActivityParty>();

            var dfa = serviceContext.DelegatedFinancialAuthority().FetchDelegatedFinancialAuthorityForTradeInForUser(appraisal.OwnerId.Id);
            if (null == dfa) return approvers;

            var toUser = serviceContext.User().FetchSystemUserById(dfa.pnl_EmailTo.Id);
            if (dfa.pnl_EmailCC != null)
            {
                var ccUser = serviceContext.User().FetchSystemUserById(dfa.pnl_EmailCC.Id);
                approvers.Add(CreateCcParty(ccUser));
            }
            approvers.Add(CreateToParty(toUser));

            return approvers;
        }

        internal static ActivityParty CreateToParty(SystemUser approver)
        {
            var toParty = new ActivityParty
            {
                PartyId = new EntityReference("systemuser", approver.Id),
                ParticipationTypeMaskEnum = activityparty_participationtypemask.ToRecipient
            };
            toParty.PartyId.Name = approver.FullName;

            return toParty;
        }

        internal static ActivityParty CreateCcParty(SystemUser approver)
        {
            var ccParty = new ActivityParty
            {
                PartyId = new EntityReference("systemuser", approver.Id),
                ParticipationTypeMaskEnum = activityparty_participationtypemask.CCRecipient
            };
            ccParty.PartyId.Name = approver.FullName;

            return ccParty;
        }
    }
}
