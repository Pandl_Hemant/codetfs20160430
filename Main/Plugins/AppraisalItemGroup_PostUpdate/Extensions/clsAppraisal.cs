﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LandPowerPlugins
{
        public interface IAppraisalExtensions
	    {
		    pnl_appraisal FetchAppraisalById(Guid appraisalId);

		    IEnumerable<pnl_appraisalitemgroup> FetchAppraisalItemGroups(Guid appraisalId);
	    }

	    public static class AppraisalExtension
	    {
		    public static Func<CrmServiceContext, IAppraisalExtensions> AppraisalFactory = serviceContext => new AppraisalExtensions(serviceContext);

		    public static IAppraisalExtensions Appraisal(this CrmServiceContext serviceContext)
		    {
			    return AppraisalFactory(serviceContext);
		    }
	    }

	    internal class AppraisalExtensions : IAppraisalExtensions
	    {
		    private readonly CrmServiceContext _serviceContext;

		    public AppraisalExtensions(CrmServiceContext serviceContext)
		    {
			    _serviceContext = serviceContext;
		    }

		    public pnl_appraisal FetchAppraisalById(Guid appraisalId)
		    {
			    return (from a in _serviceContext.pnl_appraisalSet where a.Id == appraisalId select a).FirstOrDefault();
		    }

		    public IEnumerable<pnl_appraisalitemgroup> FetchAppraisalItemGroups(Guid appraisalId)
		    {
			    var itemGroups = (from g in _serviceContext.pnl_appraisalitemgroupSet
				    where g.pnl_AppraisalId.Id == appraisalId
				          && g.statecode == pnl_appraisalitemgroupState.Active
				    select g);

			    return itemGroups;
		    }
	    }
    
}
