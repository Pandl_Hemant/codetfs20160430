﻿XrmToolbox - Early Bound Generator Settings

 * Entity dependencies:-
     * Appraisal
     * Appraisal Item
     * Appraisal Item Group
     * Appraisal Item Name
	 * Appraisal Item Picklist Item
     * Appraisal Photo
     * Customer Machine
     * Group Name
     * Machine
     * Security Role
     * SystemUserRoles
     * Team
     * TeamMembership
     * User
 

* Add Debug Non User Code:Yes
* Create One File Per Entity: No
* Generate Attribute Name Consts: No
* Generate Anonymous Type Constructors: Yes
* Generate OptionSet Enum Properties: Yes
* Use Xrm Client: No
* Service Context Name: CrmServiceContext  
* Namespace CrmEarlyBound
* Entity Realtive Path: <Project folder>\Entities.cs
