﻿function ScenerioValuesOnchange(context) {
    if (context.RowIndex == "__insert") {
        var datenow = new Date();
        parent.Xrm.Page.getAttribute("pnl_totalchanged_on").setValue(datenow);
        parent.Xrm.Page.data.entity.save();
    }
    else {
        AbleBridge.DataGrid.Events.SaveRows();
        var rowCount = AbleBridge.DataGrid.GetRowCount();
        var BookValueTotal = 0
        var Scenario2Total = 0;
        var Scenario3Total = 0;
        var OverTrade2Total = 0;
        var OverTrade3Total = 0;
        for (var i = 0; i < rowCount; i++) {
            if (AbleBridge.DataGrid.GetFieldValue('pnl_Scenario2Value', i, true) != "") {
                Scenario2Total = Scenario2Total + AbleBridge.DataGrid.GetFieldValue('pnl_Scenario2Value', i, true);
            }
        }                                                
        for (var i = 0; i < rowCount; i++) {
            if (AbleBridge.DataGrid.GetFieldValue('pnl_Scenario3Value', i, true) != "") {
                Scenario3Total = Scenario3Total + AbleBridge.DataGrid.GetFieldValue('pnl_Scenario3Value', i, true);
            }
        }
        for (var i = 0; i < rowCount; i++) {
            if (AbleBridge.DataGrid.GetFieldValue('pnl_Scenario1Value', i, true) != "") {
                BookValueTotal = BookValueTotal + AbleBridge.DataGrid.GetFieldValue('pnl_Scenario1Value', i, true);
            }
        }
        var t1s1 = 0;
        if (AbleBridge.DataGrid.GetCRMFormField('pnl_tradeinmachine1bookvalue').getValue() != null)
        {
            t1s1 = AbleBridge.DataGrid.GetCRMFormField('pnl_tradeinmachine1bookvalue').getValue();
        }
        var t2s1 = 0;
        if (AbleBridge.DataGrid.GetCRMFormField('pnl_tradeinmachine2bookvalue').getValue() != null) {
            t2s1 = AbleBridge.DataGrid.GetCRMFormField('pnl_tradeinmachine2bookvalue').getValue();
        }
        var t3s1 = 0;
        if (AbleBridge.DataGrid.GetCRMFormField('pnl_tradeinmachine3bookvalue').getValue() != null) {
            t3s1 = AbleBridge.DataGrid.GetCRMFormField('pnl_tradeinmachine3bookvalue').getValue();
        }

        var t1s2 = 0;
        if (AbleBridge.DataGrid.GetCRMFormField('pnl_tradeinmachine1scenario2value').getValue() != null) {
            t1s2 = AbleBridge.DataGrid.GetCRMFormField('pnl_tradeinmachine1scenario2value').getValue();
        }
        var t2s2 = 0;
        if (AbleBridge.DataGrid.GetCRMFormField('pnl_tradeinmachine2scenario2value').getValue() != null) {
            t2s2 = AbleBridge.DataGrid.GetCRMFormField('pnl_tradeinmachine2scenario2value').getValue();
        }
        var t3s2 = 0;
        if (AbleBridge.DataGrid.GetCRMFormField('pnl_tradeinmachine3scenario2value').getValue() != null) {
            t3s2 = AbleBridge.DataGrid.GetCRMFormField('pnl_tradeinmachine3scenario2value').getValue();
        }

        var t1s3 = 0;
        if (AbleBridge.DataGrid.GetCRMFormField('pnl_tradeinmachine1scenario3value').getValue() != null) {
            t1s3 = AbleBridge.DataGrid.GetCRMFormField('pnl_tradeinmachine1scenario3value').getValue();
        }
        var t2s3 = 0;
        if (AbleBridge.DataGrid.GetCRMFormField('pnl_tradeinmachine2scenario3value').getValue() != null) {
            t2s3 = AbleBridge.DataGrid.GetCRMFormField('pnl_tradeinmachine2scenario3value').getValue();
        }
        var t3s3 = 0;
        if (AbleBridge.DataGrid.GetCRMFormField('pnl_tradeinmachine3scenario3value').getValue() != null) {
            t3s3 = AbleBridge.DataGrid.GetCRMFormField('pnl_tradeinmachine3scenario3value').getValue();
        }

        var total1 = t1s1 + t2s1 + t3s1 + BookValueTotal;

        var total2 = t1s2 + t2s2 + t3s2 + Scenario2Total;

        var total3 = t1s3 + t2s3 + t3s3 + Scenario3Total;

        OverTrade2Total = total2 - total1;
        OverTrade3Total = total3 - total1;
         
        var datenow = new Date();
        parent.Xrm.Page.getAttribute("pnl_totalbookvalue").setValue(total1);
        parent.Xrm.Page.getAttribute("pnl_scenario2totaltradedvalue").setValue(total2);
        parent.Xrm.Page.getAttribute("pnl_scenario3totaltradedvalue").setValue(total3);
        parent.Xrm.Page.getAttribute("pnl_totalchanged_on").setValue(datenow);
        parent.Xrm.Page.data.entity.save();
    }
}

function stylechange_onLoad() {
    var cellStyle = {
        BackgroundColor: '#FFFF00'
    };
    // debugger;
    var rowCount = AbleBridge.DataGrid.GetRowCount();
    for (var i = 0; i < rowCount; i++) {
        AbleBridge.DataGrid.SetCellStyle(i, 'pnl_Scenario2Value', cellStyle);
        AbleBridge.DataGrid.SetCellStyle(i, 'pnl_Scenario3Value', cellStyle);
    }
    AbleBridge.DataGrid.Toolbar.SetNewButtonLabel("Add Existing Machine");
}

function stylechange_onInsert(context) {    
    var cellStyle = {
        BackgroundColor: '#FFFF00'
    };
    AbleBridge.DataGrid.SetCellStyle(AbleBridge.DataGrid.NewRowIndex, 'pnl_Scenario2Value', cellStyle);
    AbleBridge.DataGrid.SetCellStyle(AbleBridge.DataGrid.NewRowIndex, 'pnl_Scenario3Value', cellStyle);
    AbleBridge.DataGrid.Toolbar.SetNewButtonLabel("Add Existing Machine");

}


// populate other columns when machine is selected
function autopopulate_machinedetails(context) {
    if (context.CurrentValue.Id != null) {
        var cols = ["pnl_approvedbookvalue", "pnl_recommendedbookvalue", "pnl_appraisalstatus", "pnl_marketvalue", "pnl_sellingmarginpercentage", "pnl_repaircosts", "pnl_currentcustomerid", "pnl_appraisalstatus"];
        var retrievedMachine = parent.XrmServiceToolkit.Soap.Retrieve("pnl_machine", context.CurrentValue.Id, cols);
        if ((retrievedMachine.attributes.pnl_appraisalstatus == undefined || retrievedMachine.attributes.pnl_appraisalstatus == null) || 
            (retrievedMachine.attributes.pnl_appraisalstatus != undefined && retrievedMachine.attributes.pnl_appraisalstatus == 125760001)) {
            if (retrievedMachine.attributes.pnl_approvedbookvalue != undefined && retrievedMachine.attributes.pnl_approvedbookvalue != null){
                AbleBridge.DataGrid.SetFieldValue('pnl_Scenario1Value', retrievedMachine.attributes.pnl_approvedbookvalue.value, context.RowIndex);
                AbleBridge.DataGrid.SetFieldValue('pnl_Scenario2Value', retrievedMachine.attributes.pnl_approvedbookvalue.value, context.RowIndex);
            }
        }
        else if (retrievedMachine.attributes.pnl_recommendedbookvalue != undefined && retrievedMachine.attributes.pnl_recommendedbookvalue != null) {
            AbleBridge.DataGrid.SetFieldValue('pnl_Scenario1Value', retrievedMachine.attributes.pnl_recommendedbookvalue.value, context.RowIndex);
            AbleBridge.DataGrid.SetFieldValue('pnl_Scenario2Value', retrievedMachine.attributes.pnl_recommendedbookvalue.value, context.RowIndex);
        }
        if (retrievedMachine.attributes.pnl_marketvalue != undefined && retrievedMachine.attributes.pnl_marketvalue != null) {
            AbleBridge.DataGrid.SetFieldValue('pnl_Scenario3Value', retrievedMachine.attributes.pnl_marketvalue.value, context.RowIndex);
            //Xrm.Page.getAttribute('pnl_scenario3value').setValue(retrievedMachine.attributes.pnl_marketvalue.value);
        }
        if (retrievedMachine.attributes.pnl_marketvalue != undefined && retrievedMachine.attributes.pnl_marketvalue != null) {
            AbleBridge.DataGrid.SetFieldValue('pnl_MarketPrice', retrievedMachine.attributes.pnl_marketvalue.value, context.RowIndex);
            // Xrm.Page.getAttribute('pnl_marketprice').setValue(retrievedMachine.attributes.pnl_marketvalue.value);
        }
        if (retrievedMachine.attributes.pnl_sellingmarginpercentage != undefined && retrievedMachine.attributes.pnl_sellingmarginpercentage != null) {
            AbleBridge.DataGrid.SetFieldValue('pnl_SellingMarginpercentage', retrievedMachine.attributes.pnl_sellingmarginpercentage.value, context.RowIndex);
            // Xrm.Page.getAttribute('pnl_sellingmargin').setValue(retrievedMachine.attributes.pnl_sellingmargin.value);
        }
        if (retrievedMachine.attributes.pnl_repaircosts != undefined && retrievedMachine.attributes.pnl_repaircosts != null) {
            AbleBridge.DataGrid.SetFieldValue('pnl_RepairCost', retrievedMachine.attributes.pnl_repaircosts.value, context.RowIndex);
            //Xrm.Page.getAttribute('pnl_repaircost').setValue(retrievedMachine.attributes.pnl_repaircosts.value);
        }
        if (retrievedMachine.attributes.pnl_appraisalstatus != undefined && retrievedMachine.attributes.pnl_appraisalstatus != null) {
            AbleBridge.DataGrid.SetFieldValue('pnl_AppraisalStatus', retrievedMachine.attributes.pnl_appraisalstatus.value, context.RowIndex);
            //Xrm.Page.getAttribute('pnl_repaircost').setValue(retrievedMachine.attributes.pnl_repaircosts.value);
        }
        var configValue = getNumberOfConfig();
        if (configValue != null) {
            AbleBridge.DataGrid.SetFieldValue('pnl_Configurationid', configValue, context.RowIndex);
        }
        AbleBridge.DataGrid.Events.SaveInsertedRow();
    }
}


function machine_filter_onEdit(context) {

    //save the quote if it is dirty
    var isDirty = parent.Xrm.Page.data.entity.getIsDirty();
    if(isDirty == true)
    {
        parent.Xrm.Page.data.entity.save();
    }

    for (var i = 0; i < context.Rows.length; i++) {

        var viewName = "Active Machines"
        var viewid = "7166042F-4D8A-4CA5-808C-B36FCA7B4A6C";
        AbleBridge.DataGrid.SetInitialLookupView(context.Rows[i].RowIndex, 'pnl_Machineid', viewid);
        var cust = parent.Xrm.Page.getAttribute("customerid").getValue();
        var filter = AbleBridge.DataGrid.GetFilterExpression('pnl_CurrentCustomerId', AbleBridge.DataGrid.FilterOperators.Equals, cust[0].id);
        AbleBridge.DataGrid.FilterLookupControl(context.Rows[i].RowIndex, 'pnl_Machineid', filter);
    }
}
function machine_filter_onInsert(context) {
    //save the quote if it is dirty
    var isDirty = parent.Xrm.Page.data.entity.getIsDirty();
    if (isDirty == true) {
        parent.Xrm.Page.data.entity.save();
    }
    var viewName = "Active Machines"
    // view ID is hardcorded as it is recommended approach by Able Bridge. We can use REST commandlet provided by Able Bridge to get the viewID alternatively
    var viewid = "7166042F-4D8A-4CA5-808C-B36FCA7B4A6C";
    AbleBridge.DataGrid.SetInitialLookupView(AbleBridge.DataGrid.NewRowIndex, 'pnl_Machineid', viewid);
    var cust = parent.Xrm.Page.getAttribute("customerid").getValue();
    var filter = AbleBridge.DataGrid.GetFilterExpression('pnl_CurrentCustomerId', AbleBridge.DataGrid.FilterOperators.Equals, cust[0].id);
    AbleBridge.DataGrid.FilterLookupControl(AbleBridge.DataGrid.NewRowIndex, 'pnl_Machineid', filter);
}

//get configuration
function getNumberOfConfig() {
    var quoteid = parent.Xrm.Page.data.entity.getId();
    if (quoteid != undefined && quoteid != null) {
        var fetchXml = "<fetch mapping='logical'>" +
                             "<entity name='pnl_locallysourcedmachine'>" +
                                "<attribute name='pnl_name' />" +
								"<attribute name='pnl_config' />" +
								"<order attribute='pnl_config' descending='false' />" +
                                "<filter>" +
                                    "<condition attribute='pnl_quoteid' operator='eq' value='" + quoteid + "' />" +
                                "</filter>" +
                            "</entity>" +
                        "</fetch>";
        var lsm = parent.XrmServiceToolkit.Soap.Fetch(fetchXml);
        if (lsm != null && lsm.length == 1) {
            var value = { Id: lsm[0].id, Name: lsm[0].attributes.pnl_name.value, LogicalName: 'pnl_locallysourcedmachine' };
            return value;
        }
        else {
            return null;
        }
    }
}

//filter configuration  lookup
function config_filter_onEdit(context) {
    for (var i = 0; i < context.Rows.length; i++) {
        var viewName = "Active Locally Sourced Machines"
        var viewid = "859623FD-0C38-4A8A-BC33-224D8BD74132";
        AbleBridge.DataGrid.SetInitialLookupView(context.Rows[i].RowIndex, 'pnl_Configurationid', viewid);
        var quoteid = parent.Xrm.Page.data.entity.getId();
        //var cust = parent.Xrm.Page.getAttribute("customerid").getValue();
        var filter = AbleBridge.DataGrid.GetFilterExpression('pnl_Quoteid', AbleBridge.DataGrid.FilterOperators.Equals, quoteid);
        AbleBridge.DataGrid.FilterLookupControl(context.Rows[i].RowIndex, 'pnl_Configurationid', filter);
    }
}
function config_filter_onInsert(context) {    
    // view ID is hardcorded as it is recommended approach by Able Bridge. We can use REST commandlet provided by Able Bridge to get the viewID alternatively
    var viewName = "Active Locally Sourced Machines"
    var viewid = "859623FD-0C38-4A8A-BC33-224D8BD74132";
    AbleBridge.DataGrid.SetInitialLookupView(AbleBridge.DataGrid.NewRowIndex, 'pnl_Configurationid', viewid);
    var quoteid = parent.Xrm.Page.data.entity.getId();
    var filter = AbleBridge.DataGrid.GetFilterExpression('pnl_Quoteid', AbleBridge.DataGrid.FilterOperators.Equals, quoteid);
    AbleBridge.DataGrid.FilterLookupControl(AbleBridge.DataGrid.NewRowIndex, 'pnl_Configurationid', filter);
}



// create new machine (machine plugin creates quote-machine to populate the subgrid on quote)
function newMachine() {
    var cust = parent.Xrm.Page.getAttribute("customerid").getValue();
    var custid = cust[0].id;     
    var parameters = {};
    parameters["pnl_currentcustomerid"] = custid;
    parameters["pnl_currentcustomeridname"] = cust[0].name;
    parameters["pnl_createdfromquote"] = true;
    var quoteid = parent.Xrm.Page.data.entity.getId();
    
    parameters["pnl_quoteidmachinecreatedfromquoteid"] = quoteid;

    if (parent.Xrm.Page.getAttribute("name").getValue() != null) {
        var name = parent.Xrm.Page.getAttribute("name").getValue();
        parameters["pnl_quoteidmachinecreatedfromquoteidname"] = name;
    }
    var config = getNumberOfConfig()
    if (config != null) {
        var configid = "{" + config.Id + "}";
        parameters["pnl_lsm_machinecreatedfromquoteid"] = config.Id;
        parameters["pnl_lsm_machinecreatedfromquoteidname"] = config.Name;
    }
    parent.Xrm.Utility.openEntityForm("pnl_machine", null, parameters);
}


function savetheform(context) {

    //save the quote if it is dirty
    var datenow = new Date();
    parent.Xrm.Page.getAttribute("pnl_totalchanged_on").setValue(datenow);
        parent.Xrm.Page.data.entity.save();
}

