﻿function onLoad() {
    var nameValue = "";
    var app = Xrm.Page.getAttribute("pnl_appraisalid").getValue();
    if (app != null) {
        var fetchXml = "<fetch mapping='logical'>" +
                           "<entity name='pnl_appraisal'>" +
                              "<attribute name='pnl_name' />" +
							  "<attribute name='pnl_year' />" +
							  "<attribute name='pnl_make' />" +
                              "<attribute name='pnl_model' />" +
                              "<filter>" +
                                 "<condition attribute='pnl_appraisalid' operator='eq' value='" + app[0].id + "' />" +
                              "</filter>" +
                           "</entity>" +
                        "</fetch>";

        var retrievedApp = XrmServiceToolkit.Soap.Fetch(fetchXml);
        if (retrievedApp[0] == null) {
            return;
        }
        if (retrievedApp[0].attributes.pnl_year != undefined || retrievedAccount[0].attributes.pnl_year != null) {
            nameValue = retrievedApp[0].attributes.pnl_year.value;
        }
        else {
            nameValue = "<Not Specified>";
        }

        if (retrievedApp[0].attributes.pnl_make != undefined || retrievedAccount[0].attributes.pnl_make != null) {
            nameValue = nameValue + " " + retrievedApp[0].attributes.pnl_make.value;
        }
        else {
            nameValue = nameValue + " <Not Specified>";
        }
        if (retrievedApp[0].attributes.pnl_model != undefined || retrievedAccount[0].attributes.pnl_model != null) {
            nameValue = nameValue + " " + retrievedApp[0].attributes.pnl_model.value;
        }
        if (nameValue == "" && (retrievedApp[0].attributes.pnl_name != undefined || retrievedAccount[0].attributes.pnl_name != null)) {
            nameValue = retrievedApp[0].attributes.pnl_name.value;
        }
        else {
            nameValue = nameValue + " <Not Specified>";
        }
    }
    Xrm.Page.getAttribute("pnl_name").setValue(nameValue);
}