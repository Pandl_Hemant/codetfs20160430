﻿function changeStatustoWon() {

    var id = Xrm.Page.data.entity.getId();
    id = id.replace("{", "");
    id = id.replace("}", "");

    if (typeof (SDK) == "undefined")
    { SDK = { __namespace: true }; }
    //This will establish a more unique namespace for functions in this library. This will reduce the 
    // potential for functions to be overwritten due to a duplicate name when the library is loaded.
    SDK.SAMPLES = {
        _getServerUrl: function () {
            ///<summary>
            /// Returns the URL for the SOAP endpoint using the context information available in the form
            /// or HTML Web resource.
            ///</summary>
            var OrgServicePath = "/XRMServices/2011/Organization.svc/web";
            var serverUrl = "";
            if (typeof GetGlobalContext == "function") {
                var context = GetGlobalContext();
                serverUrl = context.getServerUrl();
            }
            else {
                if (typeof Xrm.Page.context == "object") {
                    serverUrl = Xrm.Page.context.getServerUrl();
                }
                else { throw new Error("Unable to access the server URL"); }
            }
            if (serverUrl.match(/\/$/)) {
                serverUrl = serverUrl.substring(0, serverUrl.length - 1);
            }
            return serverUrl + OrgServicePath;
        },
        WinQuoteRequest: function () {
            var requestMain = ""
            requestMain += "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">";
            requestMain += "  <s:Body>";
            requestMain += "    <Execute xmlns=\"http://schemas.microsoft.com/xrm/2011/Contracts/Services\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">";
            requestMain += "      <request i:type=\"b:WinQuoteRequest\" xmlns:a=\"http://schemas.microsoft.com/xrm/2011/Contracts\" xmlns:b=\"http://schemas.microsoft.com/crm/2011/Contracts\">";
            requestMain += "        <a:Parameters xmlns:c=\"http://schemas.datacontract.org/2004/07/System.Collections.Generic\">";
            requestMain += "          <a:KeyValuePairOfstringanyType>";
            requestMain += "            <c:key>QuoteClose</c:key>";
            requestMain += "            <c:value i:type=\"a:Entity\">";
            requestMain += "              <a:Attributes>";
            requestMain += "                <a:KeyValuePairOfstringanyType>";
            requestMain += "                  <c:key>quoteid</c:key>";
            requestMain += "                  <c:value i:type=\"a:EntityReference\">";
            requestMain += "                    <a:Id>" + id + "</a:Id>";
            requestMain += "                    <a:LogicalName>quote</a:LogicalName>";
            requestMain += "                    <a:Name i:nil=\"true\" />";
            requestMain += "                  </c:value>";
            requestMain += "                </a:KeyValuePairOfstringanyType>";
            requestMain += "                <a:KeyValuePairOfstringanyType>";
            requestMain += "                  <c:key>subject</c:key>";
            requestMain += "                  <c:value i:type=\"d:string\" xmlns:d=\"http://www.w3.org/2001/XMLSchema\">Won the Quote</c:value>";
            requestMain += "                </a:KeyValuePairOfstringanyType>";
            requestMain += "              </a:Attributes>";
            requestMain += "              <a:EntityState i:nil=\"true\" />";
            requestMain += "              <a:FormattedValues />";
            requestMain += "              <a:Id>00000000-0000-0000-0000-000000000000</a:Id>";
            requestMain += "              <a:LogicalName>quoteclose</a:LogicalName>";
            requestMain += "              <a:RelatedEntities />";
            requestMain += "            </c:value>";
            requestMain += "          </a:KeyValuePairOfstringanyType>";
            requestMain += "          <a:KeyValuePairOfstringanyType>";
            requestMain += "            <c:key>Status</c:key>";
            requestMain += "            <c:value i:type=\"a:OptionSetValue\">";
            requestMain += "              <a:Value>4</a:Value>";
            requestMain += "            </c:value>";
            requestMain += "          </a:KeyValuePairOfstringanyType>";
            requestMain += "        </a:Parameters>";
            requestMain += "        <a:RequestId i:nil=\"true\" />";
            requestMain += "        <a:RequestName>WinQuote</a:RequestName>";
            requestMain += "      </request>";
            requestMain += "    </Execute>";
            requestMain += "  </s:Body>";
            requestMain += "</s:Envelope>";
            var req = new XMLHttpRequest();
            req.open("POST", SDK.SAMPLES._getServerUrl(), true)
            // Responses will return XML. It isn't possible to return JSON.
            req.setRequestHeader("Accept", "application/xml, text/xml, */*");
            req.setRequestHeader("Content-Type", "text/xml; charset=utf-8");
            req.setRequestHeader("SOAPAction", "http://schemas.microsoft.com/xrm/2011/Contracts/Services/IOrganizationService/Execute");
            var successCallback = null;
            var errorCallback = function (error) { alert(error.message); };
            req.onreadystatechange = function () { SDK.SAMPLES.WinQuoteResponse(req, successCallback, errorCallback); };
            req.send(requestMain);
        },
        WinQuoteResponse: function (req, successCallback, errorCallback) {
            ///<summary>
            /// Recieves the assign response
            ///</summary>
            ///<param name="req" Type="XMLHttpRequest">
            /// The XMLHttpRequest response
            ///</param>
            ///<param name="successCallback" Type="Function">
            /// The function to perform when an successfult response is returned.
            /// For this message no data is returned so a success callback is not really necessary.
            ///</param>
            ///<param name="errorCallback" Type="Function">
            /// The function to perform when an error is returned.
            /// This function accepts a JScript error returned by the _getError function
            ///</param>
            if (req.readyState == 4) {
                if (req.status == 200) {
                    if (successCallback != null)
                    { successCallback(); }
                }
                else {
                    errorCallback(SDK.SAMPLES._getError(req.responseXML));
                    // SDK.SAMPLES._getError(req.responseXML);
                }
            }
        },
        _getError: function (faultXml) {
            ///<summary>
            /// Parses the WCF fault returned in the event of an error.
            ///</summary>
            ///<param name="faultXml" Type="XML">
            /// The responseXML property of the XMLHttpRequest response.
            ///</param>
            var errorMessage = "Unknown Error (Unable to parse the fault)";
            if (typeof faultXml == "object") {
                try {
                    var bodyNode = faultXml.firstChild.firstChild;
                    //Retrieve the fault node
                    for (var i = 0; i < bodyNode.childNodes.length; i++) {
                        var node = bodyNode.childNodes[i];
                        //NOTE: This comparison does not handle the case where the XML namespace changes
                        if ("s:Fault" == node.nodeName) {
                            for (var j = 0; j < node.childNodes.length; j++) {
                                var faultStringNode = node.childNodes[j];
                                if ("faultstring" == faultStringNode.nodeName) {
                                    errorMessage = faultStringNode.textContent;
                                    break;
                                }
                            }
                            break;
                        }
                    }
                }
                catch (e) { };
            }
            return new Error(errorMessage);
        },
        __namespace: true
    };
    SDK.SAMPLES.WinQuoteRequest();
    Xrm.Page.data.refresh();
    setTimeout(loadOrder, 5000);
}

function loadOrder() {

    var quoteid = Xrm.Page.data.entity.getId();
    if (quoteid != undefined && quoteid != null) {
        var fetchXml = "<fetch mapping='logical'>" +
                             "<entity name='salesorder'>" +
                                "<attribute name='quoteid' />" +
								"<attribute name='statecode' />" +
								"<attribute name='createdon' />" +
								"<order attribute='createdon' descending='true' />" +
                                "<filter>" +
                                    "<condition attribute='quoteid' operator='eq' value='" + quoteid + "' />" +
									"<condition attribute='statecode' operator='eq' value='0' />" +
                                "</filter>" +
                            "</entity>" +
                        "</fetch>";
        var quotes = XrmServiceToolkit.Soap.Fetch(fetchXml);
        if (quotes != null && quotes.length > 0) {

            if (quotes[0].attributes["statecode"] != undefined && quotes[0].attributes["statecode"] != null) {
                Xrm.Utility.openEntityForm("salesorder", quotes[0].id);
            }
            else {
                setTimeout(loadOrder, 5000);
            }
        }

    }
}

function isActive() {

    var formType = Xrm.Page.ui.getFormType();
    var satecodeOfQuote = Xrm.Page.getAttribute("statecode").getValue();
    debugger;
    if (formType == 4 && satecodeOfQuote == 1) {
        return true;
    }
    else {
        return false;
    }
}

