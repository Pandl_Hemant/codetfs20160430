﻿function rollbutton_onClick() {
    var currentDateTime = new Date();
    Xrm.Page.getAttribute("pnl_rollbackdate").setValue(currentDateTime);
    Xrm.Page.data.entity.save();
}
function getUserSecurityRole() {
    var roleName = [//"D4BC776C-6B55-42AE-B3AD-7F45D45BE640",//LIVE Product Manager - NZ
                     //"86ADFA88-02FC-4C3B-A533-F6797FC9628B",//LIVE Product Manager - AUS
                     "BFE5DF82-B029-E211-9DF3-1CC1DE79B4CA",//LIVE System Administrator - Landpower
                     "D812B114-966B-49D8-A2A9-04A8B15DADB1",//LIVE System Administrator - NZ
                     "421788C6-B91D-44E5-AD09-2543BF915C68",//LIVE System Administrator - AUS
                     //"63E8ED0D-F0D4-46DE-8AFF-3C48A524088C",//UAT Product Manager - NZ
                     //"4416FE0E-DD18-44F2-8892-3E969394B81D",//UAT Product Manager - AUS
                     "40F4A74E-FD8A-E311-9B7C-B4B52F5E28B4",//UAT System Administrator - Landpower
                     "ADF19F78-94E1-4A46-BEC5-957CA617908B",//UAT System Administrator - NZ
                     "84073AA1-5994-463C-BA29-7CF6F98299E1",//UAT System Administrator - AUS
                     //"6EC7E850-C2ED-E211-BD6A-1CC1DE6E4BB7",//ALL Product Manager - Landpower
                     "7B69A8C3-E88A-E311-8F08-D89D6779161C",//DEV1 System Administrator - Landpower
                     "24E8BEB8-EA20-4126-AFA5-202961F52F31",//DEV1 System Administrator - NZ
                     "EF7251DD-E556-412E-BAF1-83EF1836395A"];//DEV1 System Administrator - AUS
                 //"5796D128-B853-4223-9E7C-08A7B534B879",//DEV1 Product Manager - NZ
                 //"9E110A6F-C0C8-49A8-9432-B405695EAEBB"];//DEV1 Product Manager - AUS

    if (HasRole(roleName)) {
        return true;
    //Xrm.Page.ui.controls.get('pnl_salesaidapprovedstatus').setDisabled(true);
    }
    else {
        return false;
    }
}


function HasRole(roleName) {
    var roles = Xrm.Page.context.getUserRoles();

    if (roles != null) {
        for (i = 0; i < roles.length; i++) {
            for (j = 0; j < roleName.length; j++) {

                if (roles[i] == roleName[j].toLowerCase()) {
                    return true;
                }
            }
        }
    }
    return false;
}

