﻿var fieldName = "";
var setVisible = false;
var setDisablitiy = true;

function machine_onload() {
    //make_readonly();

    preFilterLookup();
    preFilterLookup_condition();
    machineType_GeneralTemplate();
    model_OtherTemplate();
    //model_readonly();
    //model_stringVisiblity();

    machineReadonly();
}

function model_OtherTemplate() {
    var cols = ["pnl_name"];
    if (Xrm.Page.getAttribute("pnl_modelid").getValue() != null) {
        var recordId = Xrm.Page.getAttribute("pnl_modelid").getValue()[0].id;
        var retrievedAppItemPicklistItem = XrmServiceToolkit.Soap.Retrieve("pnl_appraisalitempicklistitem", recordId, cols);
        var model = retrievedAppItemPicklistItem.attributes["pnl_name"];
        if (model != undefined && model != null && model.value == "Other") {
            setAttributes("pnl_modelstring", true, false);
            saveAlways();
        }
        else {
            setAttributes("pnl_modelstring", false, true);
            Xrm.Page.getAttribute("pnl_modelstring").setValue(null);
            saveAlways();
        }

    }
}
/*
function model_readonly() {
    if (Xrm.Page.getAttribute("pnl_makeid").getValue() != null && Xrm.Page.getAttribute("pnl_machinetypeid").getValue() != null) {
        setAttributes("pnl_modelid", true, false);
        setAttributes("pnl_modelstring", false, true);
        Xrm.Page.getAttribute("pnl_modelstring").setValue(null);
        saveAlways();
    }
    else if (Xrm.Page.getAttribute("pnl_makestring").getValue() != "" && Xrm.Page.getAttribute("pnl_machinetypeid").getValue() != null) {
        setAttributes("pnl_modelstring", true, false);
    }
    else {
        setAttributes("pnl_modelid", false, true);
        setAttributes("pnl_modelstring", false, true);
        Xrm.Page.getAttribute("pnl_modelstring").setValue(null);
        Xrm.Page.getAttribute("pnl_modelid").setValue(null);
        saveAlways();
    }
}

function model_stringVisiblity() {
    var cols = ["pnl_name"];
    if (Xrm.Page.getAttribute("pnl_modelid").getValue() != null) {
        var recordId = Xrm.Page.getAttribute("pnl_modelid").getValue()[0].id;
        var retrievedAppItemPicklistItem = XrmServiceToolkit.Soap.Retrieve("pnl_appraisalitempicklistitem", recordId, cols);
        var model = retrievedAppItemPicklistItem.attributes["pnl_name"];
        if (model != undefined && model != null && model.value == "Other") {
            setAttributes("pnl_modelstring", true, false);
            saveAlways();
        }
        else {
            setAttributes("pnl_modelstring", false, true);
            Xrm.Page.getAttribute("pnl_modelstring").setValue(null);
            saveAlways();
        }
    }
}

*/

function setAttributes(fieldName, setVisible, setDisablitiy) {
    if (fieldName != "") {
        Xrm.Page.getControl(fieldName).setVisible(setVisible);
        Xrm.Page.ui.controls.get(fieldName).setDisabled(setDisablitiy);
    }
}

function machineType_GeneralTemplate() {
    if (Xrm.Page.getAttribute("pnl_machinetypeid").getValue() != null) {
        var cols = ["pnl_name"];
        var recordId = Xrm.Page.getAttribute("pnl_machinetypeid").getValue()[0].id;
        var retrievedAppItemPicklistItem = XrmServiceToolkit.Soap.Retrieve("pnl_appraisalitempicklistitem", recordId, cols);
        var machineType = retrievedAppItemPicklistItem.attributes["pnl_name"];
        if (machineType != undefined && machineType != null && machineType.value == "General") {
            setAttributes("pnl_modelid", false, true);
            setAttributes("pnl_makeid", false, true);
            setAttributes("pnl_modelstring", true, false);
            setAttributes("pnl_makestring", true, false);
            saveAlways();
        }
        else {
            //setAttributes("pnl_modelstring", false, true);
            setAttributes("pnl_makestring", false, true);
            setAttributes("pnl_makeid", true, false);
            setAttributes("pnl_modelid", true, false);
            model_OtherTemplate();
            saveAlways();
        }
    }

    else {
        setAttributes("pnl_modelstring", false, true);
        setAttributes("pnl_makestring", false, true);
        setAttributes("pnl_makeid", false, true);
        setAttributes("pnl_modelid", false, true);
        Xrm.Page.getAttribute("pnl_makeid").setValue(null);
        Xrm.Page.getAttribute("pnl_makestring").setValue(null);
        Xrm.Page.getAttribute("pnl_modelid").setValue(null);
        Xrm.Page.getAttribute("pnl_modelstring").setValue(null);
        saveAlways();
    }

}

function saveAlways() {
    Xrm.Page.getAttribute("pnl_makeid").setSubmitMode("always");
    Xrm.Page.getAttribute("pnl_makestring").setSubmitMode("always");
    Xrm.Page.getAttribute("pnl_modelid").setSubmitMode("always");
    Xrm.Page.getAttribute("pnl_modelstring").setSubmitMode("always");

}



function preFilterLookup() {
    Xrm.Page.getControl("pnl_modelid").addPreSearch(function () {
        addLookupFilter();
    });
}
function addLookupFilter() {
    var machinetype = Xrm.Page.getAttribute("pnl_machinetypeid").getValue();
    var make = Xrm.Page.getAttribute("pnl_makeid").getValue();
    if (machinetype != null && make != null) {
        fetchXml = "<filter type='and'><condition attribute='pnl_filterpicklistvalue1id' operator='eq' value='" + make[0].id + "' /><condition attribute='pnl_filterpicklistvalue2id' operator='eq' value='" + machinetype[0].id + "' /></filter>";
        Xrm.Page.getControl("pnl_modelid").addCustomFilter(fetchXml);
    }
}

function preFilterLookup_condition() {
    Xrm.Page.getControl("pnl_conditionid").addPreSearch(function () {
        addLookupFilter_condition();
    });
}
function addLookupFilter_condition() {
    fetchXml = "<filter type='and'><condition attribute='pnl_appraisalitempicklistid' operator='eq' value='{E1C3900D-AF45-42F9-914B-C4EB58FA47C8}' /></filter>";
    Xrm.Page.getControl("pnl_conditionid").addCustomFilter(fetchXml);
}

function calculateBookValue() {
    var marketvalue = 0;
    var repaircosts = 0;
    var sellingmarginpercentage = 0;
    var sellingmargin = 0;
    if (Xrm.Page.getAttribute("pnl_marketvalue").getValue() != null) {
        marketvalue = Xrm.Page.getAttribute("pnl_marketvalue").getValue();
    }
    if (Xrm.Page.getAttribute("pnl_repaircosts").getValue() != null) {
        repaircosts = Xrm.Page.getAttribute("pnl_repaircosts").getValue();
    }
    if (Xrm.Page.getAttribute("pnl_sellingmarginpercentage").getValue() != null) {
        sellingmarginpercentage = Xrm.Page.getAttribute("pnl_sellingmarginpercentage").getValue();
        sellingmargin = (sellingmarginpercentage / 100) * marketvalue;
        Xrm.Page.getAttribute("pnl_sellingmargin").setValue(sellingmargin);
    }
    //if (Xrm.Page.getAttribute("pnl_sellingmargin").getValue() != null) {
    //    sellingmargin = Xrm.Page.getAttribute("pnl_sellingmarginpercentage").getValue();
    //    sellingmargin = (sellingmarginpercentage / 100) * marketvalue;
    //    Xrm.Page.getAttribute("pnl_sellingmargin").setValue(sellingmargin);
    //}
    var appBookValue = 0;
    appBookValue = marketvalue - (sellingmargin + repaircosts);

    Xrm.Page.getAttribute("pnl_approvedbookvalue").setValue(appBookValue);
}

function machineReadonly() {
    var machineId = Xrm.Page.data.entity.getId();
    if (machineId != undefined && machineId != null) {
        var fetchXml = "<fetch mapping='logical'>" +
                             "<entity name='pnl_appraisal'>" +
                                "<attribute name='pnl_name' />" +
                                "<filter>" +
                                    "<condition attribute='pnl_machineid' operator='eq' value='" + machineId + "' />" +
                                "</filter>" +
                            "</entity>" +
                        "</fetch>";
        var lsm = XrmServiceToolkit.Soap.Fetch(fetchXml);
        if (lsm != null && lsm.length > 0) {
            Xrm.Page.ui.controls.get().forEach(function (control, index) {
                control.setDisabled(true);
            });
            Xrm.Page.ui.setFormNotification('Please update the associated Appraisal to make changes to the Machine', 'INFORMATION');
        }
        else {
            Xrm.Page.ui.controls.get("pnl_recommendedbookvalue").setDisabled(true);
            //below is code to make machine read only if one of the quote assoicated with the machine is read only
            //var fetchXml = "<fetch mapping='logical'>" +
            //                 "<entity name='pnl_quotemachine'>" +
            //                    "<attribute name='pnl_name' />" +
            //                    "<filter type='and'>" +
            //                        "<condition attribute='pnl_machineid' operator='eq' value='" + machineId + "' />" +
            //                    "</filter>" +
            //                    "<link-entity name='quote' from='quoteid' to='pnl_quoteid' alias='ad'>" +
            //                    "<filter type='and'>" +
            //                        "<condition attribute='statecode' operator='eq' value='0' />" +
            //                    "</filter>" +
            //                    "</link-entity>" +
            //                "</entity>" +
            //            "</fetch>";
            //var quotes = XrmServiceToolkit.Soap.Fetch(fetchXml);
            //if (quotes != null && quotes.length > 0) {
            //    Xrm.Page.ui.controls.get().forEach(function (control, index) {
            //        control.setDisabled(true);
            //    });
            //    Xrm.Page.ui.setFormNotification('Machine is assoociated to a quote which is not in draft', 'INFORMATION');
            //}
        }
    }
}


function machineOnSave() {

    if (window.parent != null && window.parent != undefined) {
        if (window.parent.opener != null && window.parent.opener != undefined) {
            if (window.parent.opener.document != null && window.parent.opener.document != undefined) {
                if (window.parent.opener.document.location != null && window.parent.opener.document.location != undefined) {
                    window.parent.opener.document.location.reload(true);
                }
            }
        }
    }
}