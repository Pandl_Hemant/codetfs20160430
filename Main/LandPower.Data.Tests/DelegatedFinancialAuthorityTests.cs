﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xrm.Sdk;
using Moq;

namespace LandPower.Data.Tests
{
	/// <summary>
	/// This exists mostly as an illustration. Tests of the data tier are of limited value since the query execution relies on interpretation
	/// by the Linq to CRM provider.
	/// </summary>
	[TestClass]
	public class DelegatedFinancialAuthorityTests
	{
		[TestMethod]
		public void FetchTradeInTest()
		{
			var userId = new Guid("00000000-0000-0000-0000-000000000000");
			var mockService = new Mock<IOrganizationService>();
			var mockContext = new Mock<CrmServiceContext>(mockService.Object);
			// Mark pnl_delegatedfinancialauthorityset virtual in CrmServiceContext.cs to avoid "Invalid setup on a non-virtual member" error.
			// Microsoft Fakes is an alternative in VS premium/ultimate edition.
			mockContext.Setup(c => c.pnl_delegatedfinancialauthoritySet).Returns(GetFakeDelegatedFinancialAuthorities());
			var target = new DelegatedFinancialAuthorityExtensions(mockContext.Object);
			
			var actual = target.FetchDelegatedFinancialAuthorityForTradeInForUser(userId);

			Assert.IsNotNull(actual);
			Assert.IsNotNull(actual.pnl_SalesRepID);
			Assert.AreEqual(userId, actual.pnl_SalesRepID.Id);
		}

		private IQueryable<pnl_delegatedfinancialauthority> GetFakeDelegatedFinancialAuthorities()
		{
			var entity = new Entity("pnl_delegatedfinancialauthority");
			entity.Attributes.Add("pnl_salesrepid", new EntityReference("systemuser", new Guid("00000000-0000-0000-0000-000000000000")));
			entity.Attributes.Add("statecode", new OptionSetValue(0));
			entity.Attributes.Add("pnl_type", new OptionSetValue((int)pnl_delegatedfinancialauthoritytype.Tradein));

			return new List<pnl_delegatedfinancialauthority>
			{
				entity.ToEntity<pnl_delegatedfinancialauthority>()
			}.AsQueryable();
		}
	}
}
