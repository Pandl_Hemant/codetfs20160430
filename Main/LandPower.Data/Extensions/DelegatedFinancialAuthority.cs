﻿using System;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace LandPower.Data
{
	public interface IDelegatedFinancialAuthorityExtensions
	{
		pnl_delegatedfinancialauthority FetchDelegatedFinancialAuthorityForTradeInForUser(Guid userId);
		bool IsDuplicate(Guid salesRepId, int delegatedFinancialAuthorityType, Guid currentRecordId);
	}

	public static class DelegatedFinancialAuthorityExtension
	{
		// Could be internal with InternalsVisibleTo in tests project.
		public static Func<CrmServiceContext, IDelegatedFinancialAuthorityExtensions> DfaFactory = serviceContext => new DelegatedFinancialAuthorityExtensions(serviceContext);

		public static IDelegatedFinancialAuthorityExtensions DelegatedFinancialAuthority(this CrmServiceContext serviceContext)
		{
			return DfaFactory(serviceContext);
		}
	}

	internal class DelegatedFinancialAuthorityExtensions : IDelegatedFinancialAuthorityExtensions
	{
		private readonly CrmServiceContext _serviceContext;

		public DelegatedFinancialAuthorityExtensions(CrmServiceContext serviceContext)
		{
			_serviceContext = serviceContext;
		}

		public pnl_delegatedfinancialauthority FetchDelegatedFinancialAuthorityForTradeInForUser(Guid userId)
		{
			return (from d in _serviceContext.pnl_delegatedfinancialauthoritySet
					where d.pnl_SalesRepID.Id == userId
						&& d.statecode == pnl_delegatedfinancialauthorityState.Active
						&& d.pnl_Type.Value == (int)pnl_delegatedfinancialauthoritytype.Tradein // pnl_TypeEnum causes attributename error.
					select d).FirstOrDefault();
		}

		public bool IsDuplicate(Guid salesRepId, int delegatedFinancialAuthorityType, Guid currentRecordId)
		{
			// Note, Any method is not supported by CRM.
			var existingRecord = _serviceContext.pnl_delegatedfinancialauthoritySet.FirstOrDefault(d => d.pnl_SalesRepID.Id == salesRepId
			                                                                                            && d.pnl_Type.Value == delegatedFinancialAuthorityType
																										&& d.Id != currentRecordId);
			return existingRecord != null;
		}
	}
}
