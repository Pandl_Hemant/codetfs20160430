﻿using System;
using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace LandPower.Data
{
	public interface IAppraisalItemExtensions
	{
		pnl_appraisalitem FetchAppraisalItemByNameAndGroupName(Guid appraisalId, string name, string groupName);

		pnl_appraisalitem FetchAppraisalValuationItemByName(Guid appraisalId, string name);
	}

	public static class AppraisalItemExtension
	{
		public static Func<CrmServiceContext, IAppraisalItemExtensions> AppraisalItemFactory = serviceContext => new AppraisalItemExtensions(serviceContext);

		public static IAppraisalItemExtensions AppraisalItem(this CrmServiceContext serviceContext)
		{
			return AppraisalItemFactory(serviceContext);
		}
	}

	internal class AppraisalItemExtensions : IAppraisalItemExtensions
	{
		private readonly CrmServiceContext _serviceContext;

		public AppraisalItemExtensions(CrmServiceContext serviceContext)
		{
			_serviceContext = serviceContext;
		}

		public pnl_appraisalitem FetchAppraisalValuationItemByName(Guid appraisalId, string name)
		{
			// Note, can't reference a.pnl_AppraisalItemNameId.Name directly (in where clause).
			return (from a in _serviceContext.pnl_appraisalitemSet
					join g in _serviceContext.pnl_appraisalitemgroupSet on a.pnl_AppraisalItemGroupId.Id equals g.Id
					join gn in _serviceContext.pnl_groupnameSet on g.pnl_GroupNameId.Id equals gn.Id
					join n in _serviceContext.pnl_appraisalitemnameSet on a.pnl_AppraisalItemNameId.Id equals n.Id
					where a.pnl_AppraisalId.Id == appraisalId
						  && a.statecode == pnl_appraisalitemState.Active
						  && gn.pnl_name == "Valuation"
						  && n.pnl_name == name
					select a).FirstOrDefault();
		}

		public pnl_appraisalitem FetchAppraisalItemByNameAndGroupName(Guid appraisalId, string name, string groupName)
		{
			// Note, can't reference a.pnl_AppraisalItemNameId.Name directly (in where clause).
			return (from a in _serviceContext.pnl_appraisalitemSet
					join g in _serviceContext.pnl_appraisalitemgroupSet on a.pnl_AppraisalItemGroupId.Id equals g.Id
					join gn in _serviceContext.pnl_groupnameSet on g.pnl_GroupNameId.Id equals gn.Id
					join n in _serviceContext.pnl_appraisalitemnameSet on a.pnl_AppraisalItemNameId.Id equals n.Id
					where a.pnl_AppraisalId.Id == appraisalId
						  && a.statecode == pnl_appraisalitemState.Active
						  && gn.pnl_name == groupName
						  && n.pnl_name == name
					select a).FirstOrDefault();
		}
	}
}