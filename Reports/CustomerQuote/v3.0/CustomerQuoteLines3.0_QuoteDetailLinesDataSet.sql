DECLARE @pQuote uniqueidentifier;SET @pQuote = 'BE8F0D50-914B-E511-80FA-0050568A79C5'

SELECT 
	quotedetail.quoteid, 
	quotedetail.productid, 
	quotedetail.productdescription, 
	quotedetail.priceperunit, 
	quotedetail.quantity, 
	quotedetail.extendedamount, 
	quotedetail.quotedetailid, 
	IsNull(quotedetail.lineitemnumber,999999) AS lineitemnumber, 
	quotedetail.new_expudf3, 
	quotedetail.new_expudf2, 
	quotedetail.new_expudf1, 
	quotedetail.exp_linenumber, 
	quotedetail.pnl_spectypename, 
	quotedetail.pnl_islocallysourcedname, 
	quotedetail.pnl_istradein, 
	quotedetail.pnl_basepriceandselectedspecs,
	locallysourceditem.pnl_name AS locallysourceditem_pnl_name, 
	locallysourceditem.pnl_confignumber AS locallysourceditem_pnl_confignumber, 
	locallysourceditem.pnl_linenumber AS locallysourceditem_pnl_linenumber, 
	locallysourceditem.pnl_listprice AS locallysourceditem_pnl_listprice,
	quote.name AS quote_name, 
	quote.pnl_landpowerquoteid AS quote_pnl_landpowerquoteid, 
	quote.pnl_highestvaluemachinename AS quote_pnl_highestvaluemachinename, 
	quote.customerid AS quote_customerid, 
	account.primarycontactid AS account_primarycontactid
FROM Filteredquotedetail AS quotedetail
LEFT OUTER JOIN Filteredpnl_locallysourceditem AS locallysourceditem ON quotedetail.pnl_locallysourceditemid = locallysourceditem.pnl_locallysourceditemid
LEFT OUTER JOIN FilteredQuote quote ON quotedetail.quoteid = quote.quoteid
LEFT OUTER JOIN FilteredAccount account ON quote.customerid = account.accountid
WHERE quotedetail.quoteid = @pQuote
AND IsNull(exp_linenumber,locallysourceditem.pnl_confignumber)  IS NOT NULL 
ORDER BY IsNull(exp_linenumber,locallysourceditem.pnl_confignumber), IsNull(lineitemnumber,999999)


--<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">
--          <entity name="quotedetail" >
--		  <attribute name="quoteid" />
--          <attribute name="productid" />
--          <attribute name="productdescription" />
--          <attribute name="priceperunit" />
--          <attribute name="quantity" />
--          <attribute name="extendedamount" />
--          <attribute name="quotedetailid" />
--          <attribute name="lineitemnumber" />
--          <attribute name="new_expudf3" />
--          <attribute name="new_expudf2" />
--          <attribute name="new_expudf1" />
--          <attribute name="exp_linenumber" />
--          <attribute name="pnl_spectype" />
--          <attribute name="pnl_islocallysourced" />
--          <attribute name="pnl_istradein" />
--          <attribute name="pnl_basepriceandselectedspecs" />
--          <order attribute="exp_linenumber" descending="false" />
--          <order attribute="lineitemnumber" descending="false" />
--          <filter type="and">
--			<condition attribute="quoteid" operator="eq"   value="@pQuote" />
--			  <filter type="or">
--				<condition attribute="exp_linenumber" operator="not-null" />
--				<condition attribute="pnl_locallysourceditemid" operator="not-null" />
--			</filter>
--          </filter>
--		  <link-entity name="pnl_locallysourceditem" from="pnl_locallysourceditemid" to="pnl_locallysourceditemid" link-type="outer" alias="locallysourceditem">
--		  <attribute name="pnl_name" />
--		  <attribute name="pnl_confignumber" />
--          <attribute name="pnl_linenumber" />
--		  <attribute name="pnl_listprice" />
--		  </link-entity>
--          <link-entity name="quote" from="quoteid" to="quoteid" alias="quote">
--          <attribute name="name" />
--          <attribute name="pnl_landpowerquoteid" />
--          <attribute name="pnl_highestvaluemachinename" />
--          <attribute name="customerid" />
--          <link-entity name="account" from="accountid" to="customerid" visible="false" link-type="outer" alias="account">
--          <attribute name="primarycontactid" />
--          </link-entity>
--          </link-entity>
--          </entity>
--          </fetch>