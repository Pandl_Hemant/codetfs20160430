DECLARE @pQuote uniqueidentifier;
SET @pQuote = '2F54D53A-153F-E511-80FA-0050568A79C5'

SELECT 
	productdescription,
	pnl_tiserialnumber
FROM FilteredQuoteDetail
WHERE quoteid = @pQuote
AND pnl_istradein = 1
ORDER BY productdescription 



--<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">
--  <entity name="quotedetail" >
--    <attribute name="productdescription" />
--    <attribute name="pnl_tiserialnumber" />
--    <order attribute="productdescription" descending="false"/>
--    <filter type="and">
--        <condition attribute="quoteid" operator="eq" value="@pQuote" />
--    </filter>
--    <filter type="and">
--        <condition attribute="pnl_istradein" operator="eq" value="true" />
--    </filter>
--  </entity>
--</fetch>